from DAO.avis_dao import AvisDao
from DAO.cocktail_dao import CocktailDao
from DAO.composition_dao import CompositionDao
from DAO.ingredient_dao import IngredientDao

class CocktailServices():

    def __init__(self):
        pass

    @staticmethod
    def create(cocktail):
        """ Va créer un objet cocktail dans la base de doonées
            :param cocktail: le cocktail à compléter
            :type Cocktail
            :return le cocktail mis à jour de son id
            :rtype Cocktail
        """
        cocktail = CocktailDao.create(cocktail=cocktail)
        CompositionDao.create(cocktail=cocktail)
        return cocktail

    @staticmethod
    def chargement_ingredients(cocktail):
        """ Récupère la liste des ingrédients du cocktail
            :param cocktail: le cocktail à compléter
            :type Cocktail
            :return le cocktail complété
            :rtype Cocktail
        """
        cocktail = CompositionDao.completion_ingredients(cocktail=cocktail)
        cocktail = IngredientDao.completion_ingredients(cocktail=cocktail)
        return cocktail

    @staticmethod
    def chargement_avis(cocktail):
        """ Récupère la liste des avis du cocktail
            :param cocktail: le cocktail à compléter
            :type Cocktail
            :return le cocktail complété
            :rtype Cocktail
        """
        cocktail.avis = AvisDao.find_by_id_cocktail(id_cocktail=cocktail.id)
        return cocktail