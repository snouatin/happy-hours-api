class MiseEnFormeServices():

    def __init__(self):
        pass

    @staticmethod
    def mise_en_forme(liste):
        """ Mise en forme des termes de recherche possibles
            :param liste: la liste de termes à mettre en forme
            :type liste: list <string>
            :return mis_en_forme: la liste applicable aux views
            :rtype mis_en_forme: list <string>
        """
        mis_en_forme = []
        for i in range(len(liste)):
            texte = str(i+1) + '. {}'.format(liste[i])
            mis_en_forme += [texte]
        return mis_en_forme

    @staticmethod
    def mise_en_forme_ingredients(liste):
        """ Mise en forme des ingrédients qu'il est possible de rechercher
            :param liste: la liste des ingrédients à mettre en forme
            :type liste: list <Ingredient>
            :return mis_en_forme: la liste applicable aux views
            :rtype mis_en_forme: list <string>
        """
        mis_en_forme = []
        for i in range(len(liste)):
            texte = str(i+1) + '. {}'.format(liste[i].nom)
            mis_en_forme += [texte]
        return mis_en_forme

    @staticmethod
    def mise_en_forme_cocktail(cocktail):
        """ Mise en forme des ingrédients qu'il est possible de rechercher
            :param cocktail: le cocktail à mettre en forme
            :type cocktail: Cocktail
            :return mis_en_forme: le cocktail applicable aux views
            :rtype mis_en_forme: string
        """
        ingredients = ''
        j=0
        for ingredient in cocktail.ingredients :
            j+=1
            ingredients += '\n  Ingredient{}: {} , Mesure{}: {}'.format(str(j)
                                                                        , ingredient.nom
                                                                        , str(j)
                                                                        , ingredient.mesure)
        mis_en_forme = 'Cocktail: \n  Nom: {} \n  Catégorie: {} \n  Alcool: {} \n  Verre: {} {} \n  Recette: {}'.format(cocktail.nom
                                                                                                                        , cocktail.type
                                                                                                                        , cocktail.alcool
                                                                                                                        , cocktail.verre
                                                                                                                        , ingredients
                                                                                                                        , cocktail.recette)
        
        return mis_en_forme

    @staticmethod
    def mise_en_forme_avis(cocktail):
        """ Mise en forme des avis d'un cocktail
            :param avis: les avis à mettre en forme
            :type avis: list <Avis>
            :return mis_en_forme: les avis applicables aux views
            :rtype mis_en_forme: list <string>
        """
        avis = cocktail.avis
        mis_en_forme = []
        for i in range(len(avis)):
            message = str(i+1) + '. Note: {} , Commentaire: {}'.format(avis[i].note,avis[i].commentaire)
            mis_en_forme += [message]
        return mis_en_forme