from DAO.utilisateur_dao import UtilisateurDao
from DAO.favoris_dao import FavorisDao
from Business_objects.utilisateur import Utilisateur
from DAO.avis_dao import AvisDao
from DAO.avis_api_dao import AvisApiDao
from Business_objects.avis import Avis
from DAO.historique_dao import HistoriqueDao
from DAO.cocktail_dao import CocktailDao
from DAO.favoris_api_dao import FavorisApiDao
from Business_objects.cocktail import Cocktail

class UtilisateurServices():
    """ UtilisateurServices manipule les utilisateurs qui sont connectés
    """

    @staticmethod
    def update(utilisateur):
        """ Méthode qui permet l'ajout d'un favoris
            :param utilisateur: l'utilisateur
            :type utilisateur: Utilisateur
            :param cocktail: le cocktail
            :type cocktail: Cocktail
        """
        updated = UtilisateurDao.update(utilisateur=utilisateur)
        return updated

    @staticmethod
    def ajouter_favoris(utilisateur,cocktail):
        """ Méthode qui permet l'ajout d'un favoris
            :param utilisateur: l'utilisateur
            :type utilisateur: Utilisateur
            :param cocktail: le cocktail
            :type cocktail: Cocktail
        """
        if cocktail.sauvegarde == "Bdd" :
            FavorisDao.create(id_user=utilisateur.id, id_cocktail=cocktail.id)
        elif cocktail.sauvegarde == "Api" :
            FavorisApiDao.create(id_user=utilisateur.id, id_cocktail=cocktail.id)

    @staticmethod
    def delete_favoris(utilisateur,cocktail):
        """ Permet de supprimer un favoris
            :param utilisateur: l'utilisateur
            :type utilisateur: Utilisateur
            :param cocktail: le cocktail
            :type cocktail: Cocktail
        """
        test_delete = None
        if cocktail.sauvegarde == "Bdd" :
            test_delete = FavorisDao.delete(id_user=utilisateur.id,id_cocktail=cocktail.id)
        elif cocktail.sauvegarde == "Api" :
            test_delete = FavorisApiDao.delete(id_user=utilisateur.id,id_cocktail=cocktail.id)
        return test_delete

    @staticmethod
    def ajouter_avis(id_user,cocktail,note,commentaire):
        """ Permet d'ajouter un avis
            :param utilisateur: id de l'utilisateur
            :type utilisateur: int
            :param cocktail: le cocktail
            :type cocktail: Cocktail
            :param note: la note
            :type note: float
            :param commentaire: le commentaire
            :type commentaire: string
        """
        avis = Avis(id_user=id_user
                    , id_cocktail=cocktail.id
                    , note=note
                    , commentaire=commentaire)
        if cocktail.sauvegarde == "Bdd" :
            AvisDao.create(avis=avis)
        elif cocktail.sauvegarde == "Api" :
            AvisApiDao.create(avis=avis)

    @staticmethod
    def modifier_avis(cocktail,avis):
        """ Permet de modifier un avis
            :param cocktail: le cocktail
            :type cocktail: Cocktail
            :param avis: lne nouvel avis
            :type avis: Avis
        """
        if cocktail.sauvegarde == "Bdd" :
            AvisDao.update(avis=avis)
        elif cocktail.sauvegarde == "Api" :
            AvisApiDao.update(avis=avis)

    @staticmethod
    def delete_avis(avis,cocktail):
        """ Permet de supprimer un avis
            :param avis: avis à supprimer
            :type avis: Avis
            :param cocktail: cocktail auquel appartient l'avis
            :type cocktail: Cocktail
        """
        suppression_avis = None
        if cocktail.sauvegarde == "bdd" :
            suppression_avis = AvisDao.delete(avis=avis)
        elif cocktail.sauvegarde == "api" :
            suppression_avis = AvisApiDao.delete(avis=avis)
        return suppression_avis

    @staticmethod
    def delete_historique(id_historique):
        """ Permet de supprimer une recherche de la base de données
            :param id_historique: id de la recherche
            :type id_historique: int
            :return: 
            :rtype: 
        """
        HistoriqueDao.delete(id_historique=id_historique)

    @staticmethod
    def supprimer_coktail(cocktail):
        """ Permet de supprimer un cocktail
            :param cocktail: id du cocktail à supprimer
            :type cocktail:  cocktail
        """
        suppression = CocktailDao.delete(cocktail=cocktail)
        return suppression

    @staticmethod
    def accepter_suggestion(id_cocktail):
        """ Permet d'accepter une suggestion de cocktail et de l'ajouter à notre BDD
            :param id_cocktail: id du cocktail suggéré
            :type id_cocktail: int
        """
        CocktailDao.accepter_suggestion(id_cocktail=id_cocktail)

    @staticmethod
    def rejeter_suggestion(cocktail):
        """ Permet de rejeter une suggestion de cocktail et de la supprimer de notre BDD
            :param cocktail: le cocktail à retirer
            :type cocktail: Cocktail
        """
        CocktailDao.delete(cocktail=cocktail)

    @staticmethod
    def supprimer_compte(utilisateur):
        """ Permet de supprimer un utilisateur
            :param utilisateur: identifiant de l'utilisateur à supprimer
            :type utilisateur: Utilisateur
        """
        suppression = UtilisateurDao.delete(utilisateur=utilisateur)
        return suppression
