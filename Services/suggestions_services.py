from DAO.cocktail_dao import CocktailDao
from Services.cocktail_services import CocktailServices

class SuggestionsServices():

    def __init__(self):
        pass

    @staticmethod
    def get_all():
        """ Va récupérer toutes les suggestions de cocktails dans la base de données
            :param 
            :type
            :return: les cocktails en cours de validation
            :rtype: list <Cocktail>
        """
        suggestions = CocktailDao.find_suggestions()
        if suggestions :
            for cocktail in suggestions :
                cocktail = CocktailServices.chargement_ingredients(cocktail=cocktail)
        return suggestions

    @staticmethod
    def mise_en_forme(suggestions):
        """ Mise en forme d'une liste de cocktails pour les views
            :param suggestions: tous les objets cocktails qui sont en cours de validation
            :type suggestions: list <Cocktail>
            :return mise_en_forme: les cocktails sous forme de liste
            :rtype mise_en_forme: list <string>
        """
        mise_en_forme = []
        i=0
        for cocktail in suggestions :
            i+=1
            ingredients = ''
            j=0
            for ingredient in cocktail.ingredients :
                j+=1
                ingredients += ', Ingredient{}: {} '.format(str(j), ingredient.nom)
            mise_en_forme += ['{}. Nom: {} {}'.format(str(i)
                                                        , cocktail.nom
                                                        , ingredients)]
        return mise_en_forme