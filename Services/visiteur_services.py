from DAO.utilisateur_dao import UtilisateurDao
from Factory.utilisateur_factory import UtilisateurFactory

class VisiteurServices():
    """ VisiteurService manipule les utilisateurs qui ne sont pas connectés
    """

    @staticmethod
    def inscription(identifiant,password,nom,prenom,email):
        """ Méthode qui permet à un utilisateur de se connecter
            :param identifiant: identifiant de l'utilisateur
            :type identifiant: string
            :param password: mot de passe de l'utilisateur
            :type password: string
            :param nom: nom de l'utilisateur
            :type nom: string
            :param prenom: prenom de l'utilisateur
            :type prenom: string
            :param email: email de l'utilisateur
            :type email: string
            :return: compte que vient de créer l'utilisateur
            :rtype: Utilisateur
        """
        utilisateur = UtilisateurFactory.generer_utilisateur(id_user=None
                                                                , TYPE_BASE_DE_DONNEE="Regulier"
                                                                , identifiant=identifiant
                                                                , password=password
                                                                , nom=nom
                                                                , prenom=prenom
                                                                , email=email)
        utilisateur = UtilisateurDao.create(utilisateur)     
        return utilisateur


    @staticmethod
    def connexion(identifiant,password):
        """ Méthode qui permet à un utilisateur de se connecter
            :param identifiant: identifiant de l'utilisateur
            :type identifiant: string
            :param password: mot de passe de l'utilisateur
            :type password: string
            :return: compte auquel l'utilisateur vient de se connecter
            :rtype: Utilisateur
        """
        utilisateur = UtilisateurDao.find_by_identifiant(identifiant=identifiant)
        if not utilisateur :
            return None
        elif utilisateur.password == password :
            return utilisateur