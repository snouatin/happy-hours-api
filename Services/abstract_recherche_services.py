from abc import ABC, abstractmethod

class AbstractRecherche(ABC):
    """ Classe abstraite dont les classes de recherche d'un cocktail vont hériter 
        Permet d'avoir les noms des méthodes de base identiques
    """

    def __init__(self):
        """ Constructeur commun aux recherches de cocktails
            :param cocktails: cocktails qui correspondent à la recherche
            :type cocktails: list
            :param ingredients: ingredients recherchés
            :type ingredients: list
            :param alcool: la conteneur en alcool du cocktail
            :type alcool: string
            :param verre: le type de verre pour boire le cocktail
            :type verre: string
            :param type: la catégorie du cocktail
            :type type: string
        """
        pass

    @abstractmethod
    def find_by_nom_cocktail(self,nom_cocktail):
        """ Recherche un cocktail selon son nom
            :param nom_cocktail : nom du cocktail qu'on cherche
            :type nom_cocktail : string
            :return cocktail: le cocktail
            :rtype cocktail: Cocktail
        """
        return NotImplementedError

    @abstractmethod
    def find_by_nom_ingredient(self,nom_ingredient):
        """ Recherche des cocktails selon un ingrédient spécifiques
            :param nom_ingredient : nom de l'ingrédient
            :type nom_ingredient : string
            :return 
            :rtype
        """

    @abstractmethod
    def find_by_alcool(self,alcool):
        """ Recherche des cocktails selon une conteneur en alcool
            :param alcool : conteneur en alcool du cocktail
            :type alcool : string
            :return 
            :rtype
        """

    @abstractmethod
    def find_by_verre(self,verre):
        """ Recherche des cocktails qui se boivent dans un verre spécifique
            :param verre : contenant du cocktail
            :type verre : string
            :return 
            :rtype
        """

    @abstractmethod
    def find_by_type(self,type):
        """ Recherche des cocktails qui appartiennent à une certaine catégorie
            :param type : catégorie du cocktail
            :type type : string
            :return 
            :rtype
        """