from Business_objects.ingredient import Ingredient
from DAO.ingredient_dao import IngredientDao

class IngredientServices():

    def __init__(self):
        pass

    @staticmethod
    def create(nom, mesure):
        """ Méthode qui permet la création d'un objet ingrédient et l'ajoute à la base de données cet ingrédient n'y est pas
            :param nom: nom de l'ingrédient
            :type string
            :return l'ingrédient 
            :rtype Ingredient
        """
        id_ingredient = IngredientDao.find_by_nom(nom_ingredient=nom)
        if id_ingredient :
            pass
        else :
            id_ingredient = IngredientDao.create(nom_ingredient=nom)
        ingredient = Ingredient(id_ingredient=id_ingredient, nom=nom, mesure=mesure)
        return ingredient