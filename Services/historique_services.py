from DAO.historique_dao import HistoriqueDao

class HistoriqueServices():

    def __init__(self):
        pass

    @staticmethod
    def get_all():
        """ Récupère l'historique complet de l'application
            :param :
            :type :
            :return historique de l'application
            :rtype dict
        """
        historique = HistoriqueDao.get_all()
        return historique

    @staticmethod
    def add_historique(id_user,terme):
        """ Ajoute une recherche dans la base de données
            :param terme: la recherche à ajouter
            :type terme: string
            :return 
            :rtype 
        """
        id_historique = HistoriqueDao.create(id_user=id_user,terme_recherche=terme)
        if id_historique :
            return True
        else :
            return False

    @staticmethod
    def mise_en_forme(historique):
        """ Met en forme un hitorique pour pouvoir l'utiliser dans les views
            :param historique
            :type dict
            :return historique mis en forme
            :rtype list <string>
        """
        historique_mis_en_forme = []
        for indice in range(len(historique)) :
            id_user = historique[indice]["id_user"]
            terme = historique[indice]["terme_recherche"]
            historique_mis_en_forme += [str(indice+1) + '. Utilisateur id: {} , Terme de recherche: {}'.format(id_user,terme)]
        return historique_mis_en_forme