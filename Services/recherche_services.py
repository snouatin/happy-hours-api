from Services.abstract_recherche_services import AbstractRecherche

from Services.recherche_api_services import RechercheAPIServices
from Services.recherche_bdd_services import RechercheBDDServices

class RechercheServices(AbstractRecherche):

    def __init__(self):
        """ Initialisation de la recherche
        """
        self.recherche_bdd = RechercheBDDServices()
        self.recherche_api = RechercheAPIServices()

    def find_by_nom_cocktail(self,nom_cocktail):
        cocktail = None
        if self.recherche_bdd.liste_cocktails != None :
            cocktail_return = self.recherche_bdd.find_by_nom_cocktail(nom_cocktail=nom_cocktail)
            if cocktail_return :
                cocktail = cocktail_return
        if self.recherche_api.liste_cocktails != None :
            cocktail_return = self.recherche_api.find_by_nom_cocktail(nom_cocktail=nom_cocktail)
            if cocktail_return :
                cocktail = cocktail_return
        return cocktail

    def find_by_nom_ingredient(self,nom_ingredient):
        if self.recherche_bdd.liste_cocktails != None :
            self.recherche_bdd.find_by_nom_ingredient(nom_ingredient=nom_ingredient)
        if self.recherche_api.liste_cocktails != None :
            self.recherche_api.find_by_nom_ingredient(nom_ingredient=nom_ingredient)

    def find_by_alcool(self,alcool):
        if self.recherche_bdd.liste_cocktails != None :
            self.recherche_bdd.find_by_alcool(alcool=alcool)
        if self.recherche_api.liste_cocktails != None :
            self.recherche_api.find_by_alcool(alcool=alcool)

    def find_by_verre(self,verre):
        if self.recherche_bdd.liste_cocktails != None :
            self.recherche_bdd.find_by_verre(verre=verre)
        if self.recherche_api.liste_cocktails != None :
            self.recherche_api.find_by_verre(verre=verre)

    def find_by_type(self,type):
        if self.recherche_bdd.liste_cocktails != None :
            self.recherche_bdd.find_by_type(type=type)
        if self.recherche_api.liste_cocktails != None :
            self.recherche_api.find_by_type(type=type)

    def liste_cocktails(self):
        cocktails = []
        if self.recherche_bdd.liste_cocktails != None :
            for cocktail in self.recherche_bdd.liste_cocktails :
                cocktails += [cocktail]
        if self.recherche_api.liste_cocktails != None :
            for cocktail in self.recherche_api.liste_cocktails :
                if cocktail not in cocktails :
                    cocktails += [cocktail]
        return cocktails

    def liste_ingredients(self):
        ingredients = []
        if self.recherche_bdd.liste_cocktails != None :
            for ingredient in self.recherche_bdd.liste_ingredients :
                ingredients += [ingredient]
        if self.recherche_api.liste_cocktails != None :
            for ingredient in self.recherche_api.liste_ingredients :
                if ingredient not in ingredients :
                    ingredients += [ingredient]
        return ingredients

    def liste_alcools(self):
        alcools = []
        if self.recherche_bdd.liste_cocktails != None :
            for alcool in self.recherche_bdd.liste_alcool :
                alcools += [alcool]
        if self.recherche_api.liste_cocktails != None :
            for alcool in self.recherche_api.liste_alcool :
                if alcool not in alcools :
                    alcools += [alcool]
        return alcools

    def liste_verres(self):
        verres = []
        if self.recherche_bdd.liste_cocktails != None :
            for verre in self.recherche_bdd.liste_verre :
                verres += [verre]
        if self.recherche_api.liste_cocktails != None :
            for verre in self.recherche_api.liste_verre :
                if verre not in verres :
                    verres += [verre]
        return verres

    def liste_types(self):
        types = []
        if self.recherche_bdd.liste_cocktails != None :
            for type in self.recherche_bdd.liste_type :
                types += [type]
        if self.recherche_api.liste_cocktails != None :
            for type in self.recherche_api.liste_type :
                if type not in types :
                    types += [type]
        return types