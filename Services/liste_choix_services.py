from API.api import Api

class ListeChoixServices():

    def __init__(self):
        pass

    @staticmethod
    def alcool():
        """ Récupère la liste des différentes conteneurs en alcool
            :param 
            :type
            :return la liste des conteneurs
            :rtype list <string>
        """
        liste_alcools = Api.find_tous_alcools()
        return liste_alcools

    @staticmethod
    def verre():
        """ Récupère la liste des différents verres
            :param 
            :type
            :return la liste des verres
            :rtype list <string>
        """
        liste_verres = Api.find_tous_verres()
        return liste_verres

    @staticmethod
    def categorie():
        """ Récupère la liste des différentes catégories de cocktails
            :param 
            :type
            :return la liste des catégories
            :rtype list <string>
        """
        liste_types = Api.find_tous_types()
        return liste_types