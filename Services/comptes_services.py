from DAO.utilisateur_dao import UtilisateurDao

class ComptesServices():

    def __init__(self):
        pass

    @staticmethod
    def get_all():
        """ Récupère les comptes de tous les utilisateurs
            :param
            :type
            :return Les comptes de tous les utilisateurs
            :rtype list <Utilisateur>
        """
        comptes_utilisateurs = UtilisateurDao.get_all()
        return comptes_utilisateurs

    @staticmethod
    def mise_en_forme(comptes_utilisateurs):
        """ Mise en forme des comptes des utilisateurs pour la view
            :param comptes_utilisateurs: tous les comptes des utilisateurs
            :type list <Utilisateur>
            :return: les comptes sous forme de liste
            :rtype: list <string>
        """
        mise_en_forme = []
        i=0
        for utilisateur in comptes_utilisateurs :
            i += 1
            mise_en_forme += ['{}. Utilisateur id: {} , Rang: {} , Nom: {} , Prenom: {} , Email: {}'.format(str(i)
                                                                                                            , utilisateur.id
                                                                                                            , utilisateur.type
                                                                                                            , utilisateur.nom
                                                                                                            , utilisateur.prenom
                                                                                                            , utilisateur.email)]
        return mise_en_forme