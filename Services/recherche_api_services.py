from Services.abstract_recherche_services import AbstractRecherche
from Services.liste_choix_services import ListeChoixServices
from API.api import Api

class RechercheAPIServices(AbstractRecherche):

    def __init__(self):
        """ Initialisation des attributs de l'objet recherche
        """
        self.liste_cocktails = []
        self.liste_ingredients = []
        self.liste_type = ListeChoixServices.categorie()
        self.final_type = None
        self.liste_verre = ListeChoixServices.verre()
        self.finale_verre = None
        self.liste_alcool = ListeChoixServices.alcool()
        self.final_alcool = None
        self.termes = []

    def find_by_nom_cocktail(self, nom_cocktail):
        """ Effectue une recherche de cocktail
            :param nom_cocktail: nom du cocktail que l'utilisateur veut consulter
            :type nom_cocktail: string
            :return cocktail: le cocktail qui porte ce nom
            :rtype cocktail: Cocktail
        """
        self.termes += [nom_cocktail]
        found_cocktail = None

        if len(self.liste_cocktails) > 0 : 
            for cocktail in self.liste_cocktails :
                if cocktail.nom == nom_cocktail :
                    found_cocktail = cocktail

        else :
            cocktail = Api.find_objet_by_nom(nom_cocktail=nom_cocktail)
            if cocktail :
                found_cocktail = cocktail
        
        return found_cocktail

    def find_by_nom_ingredient(self, nom_ingredient):
        """ Effectue une recherche de cocktails
            :param nom_ingredient: nom d'un ingrédient que l'utilisateur veut dans son cocktail
            :type nom_ingredient: string
            :return 
            :rtype
        """
        self.termes += [nom_ingredient]
        cocktails = Api.find_by_nom_ingredient(nom_ingredient=nom_ingredient)

        if cocktails :

            if self.liste_cocktails == [] :
                self.liste_cocktails += cocktails

            elif self.liste_cocktails and len(self.liste_cocktails) > 0 :
                nouvelle_liste = []
                id_cocktails = []
                for cocktail in cocktails :
                    id_cocktails += [cocktail.id]
                for cocktail in self.liste_cocktails :
                    if cocktail.id in id_cocktails :
                        nouvelle_liste += [cocktail]
                self.liste_cocktails = nouvelle_liste

            liste_ingredients = []
            liste_alcools = []
            liste_types = []
            liste_verres = []
            for cocktail in self.liste_cocktails :
                if cocktail.type not in liste_types :
                    liste_types += [cocktail.type]
                if cocktail.alcool not in liste_alcools :
                    liste_alcools += [cocktail.alcool]
                if cocktail.verre not in liste_verres :
                    liste_verres += [cocktail.verre]
                for ingredient in cocktail.ingredients :
                    if ingredient.nom not in liste_ingredients :
                        liste_ingredients += [ingredient.nom]
            for terme in self.termes :
                if terme in liste_ingredients :
                    liste_ingredients.remove(terme)
                if terme in liste_alcools :
                    liste_alcools.remove(terme)
                if terme in liste_verres :
                    liste_verres.remove(terme)
                if terme in liste_types :
                    liste_types.remove(terme)
            self.liste_type = liste_types
            self.liste_alcool = liste_alcools
            self.liste_verre = liste_verres

        else :
            self.liste_cocktails = None

    def find_by_alcool(self, alcool):
        """ Effectue une recherche de cocktails
            :param alcool: conteneur en alcool d'un cocktail
            :type alcool: string
            :return 
            :rtype
        """
        self.final_alcool = alcool
        self.termes += [alcool]
        cocktails = Api.find_by_alcool(alcool=alcool)

        if cocktails :

            if self.liste_cocktails == [] :
                self.liste_cocktails += cocktails

            elif self.liste_cocktails and len(self.liste_cocktails) > 0 :
                nouvelle_liste = []
                for cocktail in self.liste_cocktails :
                    keep = False
                    for found_cocktail in cocktails :
                        if cocktail.id == found_cocktail.id :
                            keep = True
                    if keep :
                        nouvelle_liste += [cocktail]
                self.liste_cocktails = nouvelle_liste

            liste_ingredients = []
            liste_types = []
            liste_verres = []
            for cocktail in self.liste_cocktails :
                if cocktail.type not in liste_types :
                    liste_types += [cocktail.type]
                if cocktail.verre not in liste_verres :
                    liste_verres += [cocktail.verre]
                for ingredient in cocktail.ingredients :
                    if ingredient.nom not in liste_ingredients :
                        liste_ingredients += [ingredient.nom]
            for terme in self.termes :
                if terme in liste_ingredients :
                    liste_ingredients.remove(terme)
                if terme in liste_verres :
                    liste_verres.remove(terme)
                if terme in liste_types :
                    liste_types.remove(terme)
            self.liste_type = liste_types
            self.liste_verre = liste_verres

        else :
            self.liste_cocktails = None

    def find_by_verre(self, verre):
        """ Effectue une recherche de cocktails
            :param verre: certain type de contenant des cocktails
            :type verre: string
            :return 
            :rtype
        """
        self.final_verre = verre
        self.termes += [verre]
        cocktails = Api.find_by_verre(verre=verre)

        if cocktails :

            if self.liste_cocktails == [] :
                self.liste_cocktails += cocktails

            elif self.liste_cocktails and len(self.liste_cocktails) > 0 :
                nouvelle_liste = []
                for cocktail in self.liste_cocktails :
                    keep = False
                    for found_cocktail in cocktails :
                        if cocktail.id == found_cocktail.id :
                            keep = True
                    if keep :
                        nouvelle_liste += [cocktail]
                self.liste_cocktails = nouvelle_liste

            liste_ingredients = []
            liste_types = []
            liste_alcools = []
            for cocktail in self.liste_cocktails :
                if cocktail.type not in liste_types :
                    liste_types += [cocktail.type]
                if cocktail.alcool not in liste_alcools :
                    liste_alcools += [cocktail.alcool]
                for ingredient in cocktail.ingredients :
                    if ingredient.nom not in liste_ingredients :
                        liste_ingredients += [ingredient.nom]
            for terme in self.termes :
                if terme in liste_ingredients :
                    liste_ingredients.remove(terme)
                if terme in liste_alcools :
                    liste_alcools.remove(terme)
                if terme in liste_types :
                    liste_types.remove(terme)
            self.liste_type = liste_types
            self.liste_alcool = liste_alcools

        else :
            self.liste_cocktails = None

    def find_by_type(self, type):
        """ Effectue une recherche de cocktails
            :param type: certaine catégorie de cocktails
            :type type: string
            :return 
            :rtype
        """
        self.final_type = type
        self.termes += [type]
        cocktails = Api.find_by_type(type=type)

        if cocktails :

            if self.liste_cocktails == [] :
                self.liste_cocktails += cocktails

            elif self.liste_cocktails and len(self.liste_cocktails) > 0 :
                nouvelle_liste = []
                for cocktail in self.liste_cocktails :
                    keep = False
                    for found_cocktail in cocktails :
                        if cocktail.id == found_cocktail.id :
                            keep = True
                    if keep :
                        nouvelle_liste += [cocktail]
                self.liste_cocktails = nouvelle_liste

            liste_ingredients = []
            liste_verres = []
            liste_alcools = []
            for cocktail in self.liste_cocktails :
                if cocktail.verre not in liste_verres :
                    liste_verres += [cocktail.verre]
                if cocktail.alcool not in liste_alcools :
                    liste_alcools += [cocktail.alcool]
                for ingredient in cocktail.ingredients :
                    if ingredient.nom not in liste_ingredients :
                        liste_ingredients += [ingredient.nom]
            for terme in self.termes :
                if terme in liste_ingredients :
                    liste_ingredients.remove(terme)
                if terme in liste_alcools :
                    liste_alcools.remove(terme)
                if terme in liste_verres :
                    liste_verres.remove(terme)
            self.liste_alcool = liste_alcools
            self.liste_verre = liste_verres

        else :
            self.liste_cocktails = None