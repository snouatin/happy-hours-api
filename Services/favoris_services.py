from DAO.favoris_api_dao import FavorisApiDao
from DAO.favoris_dao import FavorisDao
from DAO.cocktail_dao import CocktailDao
from DAO.composition_dao import CompositionDao
from DAO.ingredient_dao import IngredientDao
from API.api import Api

class FavorisServices():

    def __init__(self):
        pass

    @staticmethod
    def chargement(utilisateur):
        liste_favoris = []
        favoris_bdd = utilisateur.favoris_bdd
        if favoris_bdd :
            for favoris in favoris_bdd :
                favoris = CocktailDao.find_objet(id_cocktail=favoris)
                favoris = CompositionDao.completion_ingredients(cocktail=favoris)
                favoris = IngredientDao.completion_ingredients(cocktail=favoris)
                liste_favoris += [favoris]
        favoris_api = utilisateur.favoris_api
        if favoris_api :
            for favoris in favoris_api :
                favoris = Api.find_objet(id_cocktail=favoris)
                liste_favoris += [favoris]
        return liste_favoris

    @staticmethod
    def mise_en_forme(favoris):
        """ Permet de mettre en forme les favoris d'un utilisateur pour les views
            :param favoris: les favoris d'un utilisateur
            :type favoris: list <Cocktail>
            :return: les favoris sous forme d'une liste
            :rtype: liste
        """
        mis_en_forme = []
        i=0
        for cocktail in favoris :
            i+=1
            ingredients = ''
            j=0
            for ingredient in cocktail.ingredients :
                j+=1
                ingredients += ', Ingredient{}: {} '.format(str(j), ingredient.nom)
            mis_en_forme += ['{}. Nom: {} {}'.format(str(i)
                                                        , cocktail.nom
                                                        , ingredients)]
        return mis_en_forme