import os
clear = lambda: os.system('cls')

def nom_format(texte):

    taille = len(texte)
    caracteres = list(texte)
    caracteres[0] = caracteres[0].upper()
    maj = False
    for i in range(1, taille):
        if maj :
            caracteres[i] = caracteres[i].upper()
            maj = False
        else :
            caracteres[i] = caracteres[i].lower()
        if caracteres[i] == " ":
            maj = True
    texte = ''.join(caracteres)

    return texte