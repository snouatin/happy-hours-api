import psycopg2

from DAO.abstract_dao import AbstractDao
from DAO.pool_connection import PoolConnection
from Business_objects.cocktail import Cocktail
from DAO.ingredient_dao import IngredientDao
from Factory.ingredient_factory import IngredientFactory

class CompositionDao(AbstractDao):

    @staticmethod
    def create(cocktail):
        """ Ajoute une composition dans notre base de données
            :param cocktail: le cocktail dont on ajoute la composition
            :type cocktail: Cocktail
        """
        created = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            for ingredient in cocktail.ingredients :
                curseur.execute(
                    "INSERT INTO compositions (id_cocktail, id_ingredient, mesure) "
                    "VALUES (%s, %s, %s) ;"
                    , (cocktail.id
                        , ingredient.id
                        , ingredient.mesure) )
            if curseur.rowcount > 0:
                created = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # La transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return created

    @staticmethod
    def update(cocktail):
        """ Met a jour une composition dans la base de donnees
            :param cocktail: le cocktail dont on met a jour la composition
            :type cocktail: Cocktail
            :return: si la mise à jour a ete faite
            :rtype: bool
        """
        updated = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            for ingredient in cocktail.ingredients :
                curseur.execute(
                    "UPDATE compositions "
                    "SET id_ingredient = %s, mesure = %s "
                    "WHERE id_cocktail = %s ;"
                    , (ingredient.id
                        , ingredient.mesure
                        , cocktail.id) )
            if curseur.rowcount > 0:
                updated = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return updated

    @staticmethod
    def completion_ingredients(cocktail):
        """ Crée la liste d'objets ingrédients d'un cocktail mais sans les noms
            :param cocktail : cocktail à compléter
            :type cocktail : Cocktail
            :return cocktail complété
            :rtype Cocktail
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_ingredient, mesure "
                "FROM compositions " 
                "WHERE id_cocktail = %s ;"
                , (cocktail.id, )
            )
            resultats = curseur.fetchall()
            # Si on a un résultat
            if resultats :
                cocktail.ingredients = []
                for row in resultats :
                    id_ingredient = row[0]
                    mesure = row[1]
                    ingredient = IngredientFactory.generer_ingredient(id_ingredient=id_ingredient
                                                                            , nom_ingredient=None
                                                                            , mesure=mesure)
                    cocktail.ingredients += [ingredient]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
            
        return cocktail

    @staticmethod
    def find_by_nom_ingredient(nom_ingredient):
        """ Crée une liste d'id des cocktails qui contiennent un certain ingrédient
            :param nom_ingredient: nom de l'ingrédient
            :type nom_ingredient: string
            :return liste des id des cocktails concernés
            :rtype list <int>
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT compositions.id_cocktail "
                "FROM compositions " 
                "JOIN ingredients ON ingredients.id_ingredient = compositions.id_ingredient "
                "JOIN cocktails ON cocktails.id_cocktail = compositions.id_cocktail "
                "WHERE ingredients.nom_ingredient = %s AND cocktails.statut = true ;"
                , (nom_ingredient, )
            )
            resultats = curseur.fetchall()
            liste_id = None
            # Si on a un résultat
            if resultats :
                liste_id = []
                for row in resultats :
                    id_cocktail = row[0]
                    liste_id += [int(id_cocktail)]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        
        return liste_id