import psycopg2

from DAO.abstract_dao import AbstractDao
from DAO.pool_connection import PoolConnection
from Business_objects.utilisateur import Utilisateur
from Factory.utilisateur_factory import UtilisateurFactory

class UtilisateurDao(AbstractDao):

    @staticmethod
    def create(utilisateur):
        """ Ajoute un utilisateur dans notre base de données
            :param utilisateur: l'utilisateur à ajouter
            :type utilisateur: Utilisateur
            :return: l'utilisateur mis à jour de son id
            :rtype: Utilisateur
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "INSERT INTO utilisateurs (type_user, identifiant, pass_word, nom, prenom, email) "
                "VALUES (%s, %s, %s, %s, %s, %s) RETURNING id_user ;"
                , (utilisateur.type
                   , utilisateur.identifiant
                   , utilisateur.password
                   , utilisateur.nom
                   , utilisateur.prenom
                   , utilisateur.email)
            )
            # On récupère l'id généré
            utilisateur.id = curseur.fetchone()[0]
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
            
        return utilisateur

    @staticmethod
    def delete(utilisateur):
        """ Supprime un utilisateur de la base
            :param utilisateur : l'utilisateur à supprimer
            :type utilisateur: Utilisateur
            :return: si la suppresion a été faite
            :rtype: bool
        """
        deleted = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "DELETE FROM utilisateurs WHERE id_user = %s ;"
                , (utilisateur.id, ) )
            # On verifie s'il y a eu des supressions
            if curseur.rowcount > 0:
                deleted = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return deleted

    @staticmethod
    def update(utilisateur):
        """ Met à jour un utilisateur dans la base de données avec l'id
            :param utilisateur: 
            :type utilisateur: Utilisateur
            :return: si la mise à jour a été faite
            :rtype: bool
        """
        updated = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "UPDATE utilisateurs "
                "SET type_user = %s, identifiant = %s, pass_word = %s, nom  = %s, prenom  = %s, email  = %s "
                "WHERE id_user = %s ;"
                , (utilisateur.type
                   , utilisateur.identifiant
                   , utilisateur.password
                   , utilisateur.nom
                   , utilisateur.prenom
                   , utilisateur.email
                   , utilisateur.id)
            )
            if curseur.rowcount > 0:
                updated = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return updated

    @staticmethod
    def find_by_identifiant(identifiant):
        """ Trouve un utilisateur dont on connait l'identifiant
            :param identifiant : l'identifiant de l'utilisateur qu'on cherche
            :type identifiant : string
            :return l'utilisateur avec le bon identifiant
            :rtype Utilisateur
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_user, type_user, pass_word, nom, prenom, email "
                "FROM utilisateurs WHERE identifiant = %s ;"
                , (identifiant,)
            )
            resultat = curseur.fetchone()
            utilisateur = None
            # Si on a un résultat
            if resultat:
                utilisateur = UtilisateurFactory.generer_utilisateur(id_user = resultat[0]
                                                                        , TYPE_BASE_DE_DONNEE = resultat[1]
                                                                        , identifiant = identifiant
                                                                        , password = resultat[2]
                                                                        , nom = resultat[3]
                                                                        , prenom = resultat[4]
                                                                        , email = resultat[5])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return utilisateur

    @staticmethod
    def find_by_id_user(id_user):
        """ Trouve un utilisateur dont on connait l'id
            :param identifiant : l'identifiant de l'utilisateur qu'on cherche
            :type identifiant : string
            :return l'utilisateur avec le bon identifiant
            :rtype Utilisateur
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT identifiant, type_user, pass_word, nom, prenom, email "
                "FROM utilisateurs WHERE id_user = %s ;"
                , (id_user, )
            )
            resultat = curseur.fetchone()
            utilisateur = None
            # Si on a un résultat
            if resultat:
                utilisateur = UtilisateurFactory.generer_utilisateur(id_user = id_user
                                                                        , TYPE_BASE_DE_DONNEE = resultat[0]
                                                                        , identifiant = resultat[1]
                                                                        , password = resultat[2]
                                                                        , nom = resultat[3]
                                                                        , prenom = resultat[4]
                                                                        , email = resultat[5])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return utilisateur

    @staticmethod
    def get_all():
        """ Recupère tous les utilisateurs de la base de données
            :param :
            :type :
            :return tous les utilisateurs
            :rtype list <Utilisateur>
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT * "
                "FROM utilisateurs ;"
            )
            resultats = curseur.fetchall()
            liste = []
            # Si on a un résultat
            if resultats:
                for row in resultats :
                    utilisateur = UtilisateurFactory.generer_utilisateur(id_user = row[0]
                                                                            , TYPE_BASE_DE_DONNEE = row[1]
                                                                            , identifiant = row[2]
                                                                            , password = row[3]
                                                                            , nom = row[4]
                                                                            , prenom = row[5]
                                                                            , email = row[6])
                    liste += [utilisateur]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return liste