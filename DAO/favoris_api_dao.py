import psycopg2

from DAO.abstract_dao import AbstractDao
from DAO.pool_connection import PoolConnection

class FavorisApiDao(AbstractDao):

    @staticmethod
    def create(id_user, id_cocktail):
        """ Ajoute un favoris dans la base de donnees
            :param id_user: id de l'utilisateur qui ajoute le favoris
            :type id_user: int
            :param id_cocktail: le terme de la recherche
            :type id_cocktail: int
        """
        created = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "INSERT INTO favoris_api (id_user, id_cocktail_api) "
                " VALUES (%s, %s) ;"
                , (id_user
                   , id_cocktail)
            )
            if curseur.rowcount > 0 :
                created = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return created

    @staticmethod
    def delete(id_user, id_cocktail):
        """ Supprime un favoris de la base
            :param id_user : id de l'utilisateur
            :type id_user: int
            :param id_cocktail : id du cocktail
            :type id_cocktail: int
            :return: si la suppresion a été faite
            :rtype: bool
        """
        deleted = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "DELETE FROM favoris_api WHERE id_user = %s AND id_cocktail_api = %s ;"
                , (id_user
                    , id_cocktail) )
            # On verifie s'il y a eu des supressions
            if curseur.rowcount > 0:
                deleted = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return deleted

    @staticmethod
    def find_by_id_user(id_user):
        """ Trouve les favoris d'un utilisateur
            :param id_user : id de l'utilisateur
            :type id_user : int
            :return favoris de l'utilisateur
            :rtype list
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_cocktail_api "
                "FROM favoris_api WHERE id_user = %s ;"
                , (id_user, ) )
            resultats = curseur.fetchall()  
            favoris = None
            if resultats :
                favoris = []
                for row in resultats :
                    favoris += [row[0]]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return favoris