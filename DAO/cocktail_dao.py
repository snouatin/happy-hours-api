import psycopg2

from DAO.abstract_dao import AbstractDao
from DAO.pool_connection import PoolConnection
from Factory.cocktail_factory import CocktailFactory

class CocktailDao(AbstractDao):

    @staticmethod
    def create(cocktail):
        """ Ajoute un cocktail dans notre base de données
            :param cocktail: le cocktail a ajouter
            :type cocktail: Cocktail
            :return: le cocktail mis a jour de son id
            :rtype: Cocktail
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On cree le cocktail dans la table des cocktails
            curseur.execute(
                "INSERT INTO cocktails (nom_cocktail, type, alcool, verre, recette, statut) "
                " VALUES (%s, %s, %s, %s, %s, %s) RETURNING id_cocktail ;"
                , (cocktail.nom
                    , cocktail.type
                    , cocktail.alcool
                    , cocktail.verre
                    , cocktail.recette
                    , False)
            )
            # On recupere l'id genere
            cocktail.id = curseur.fetchone()[0]
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # La transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
            
        return cocktail

    @staticmethod
    def delete(cocktail):
        """ Supprime un cocktail de la base
            :param cocktail : le cocktail a supprimer
            :type cocktail: Cocktail
            :return: si la suppresion a ete faite
            :rtype: bool
        """
        deleted = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "DELETE FROM cocktails WHERE id_cocktail = %s ;"
                , (cocktail.id, ) )
            # On verifie s'il y a eu des supressions
            if curseur.rowcount > 0:
                deleted = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # La transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return deleted

    @staticmethod
    def update(cocktail):
        """ Met a jour un cocktail dans la base de données avec l'id
            :param cocktail: 
            :type cocktail: Cocktail
            :return: si la mise à jour a ete faite
            :rtype: bool
        """
        updated = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "UPDATE cocktails "
                "SET nom_cocktail = %s, type = %s, alcool = %s, verre = %s, recette = %s "
                "WHERE id_cocktail = %s ;"
                , (cocktail.nom
                   , cocktail.type
                   , cocktail.alcool
                   , cocktail.verre
                   , cocktail.recette
                   , cocktail.id)
            )
            if curseur.rowcount > 0:
                updated = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return updated

    @staticmethod
    def accepter_suggestion(id_cocktail):
        """ Passe de false à true le statut du cocktail et le rend accessible pour les utilisateurs
            :param id_cocktail
            :type cocktail: int
            :return: si la mise à jour a ete faite
            :rtype: bool
        """
        updated = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "UPDATE cocktails "
                "SET statut = true "
                "WHERE id_cocktail = %s ;"
                , (id_cocktail, )
            )
            if curseur.rowcount > 0:
                updated = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return updated

    @staticmethod
    def find_suggestions():
        """ Trouve les cocktails en attente
            :param 
            :type 
            :return les id des cocktails
            :rtype list
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        liste = None
        try:
            curseur.execute(
                "SELECT id_cocktail, nom_cocktail, type, alcool, verre, recette "
                "FROM cocktails WHERE statut = False ;"
            )
            resultats = curseur.fetchall()
            # Si on a un résultat
            if resultats :
                liste = []
                for row in resultats :
                    cocktail = CocktailFactory.generer_cocktail_bdd(id_cocktail=row[0]
                                                                    , nom_cocktail=row[1]
                                                                    , type=row[2]
                                                                    , alcool=row[3]
                                                                    , verre=row[4]
                                                                    , recette=row[5]
                                                                    , sauvegarde="Bdd")
                    liste += [cocktail]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return liste

    @staticmethod
    def find_objet(id_cocktail):
        """ Trouve un objet cocktail dans la base de données
            :param id_cocktail: id du cocktail
            :type id_cocktail: int
            :return cocktail: le cocktail amputé de ses ingrédients
            :rtype Cocktail
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        cocktail = None
        try:
            curseur.execute(
                "SELECT nom_cocktail, type, alcool, verre, recette "
                "FROM cocktails WHERE id_cocktail = %s AND statut = True ;"
                , (id_cocktail, )
            )
            resultat = curseur.fetchone()
            # Si on a un résultat
            if resultat :
                cocktail = CocktailFactory.generer_cocktail_bdd(id_cocktail=id_cocktail
                                                                , nom_cocktail=resultat[0]
                                                                , type=resultat[1]
                                                                , alcool=resultat[2]
                                                                , verre=resultat[3]
                                                                , recette=resultat[4]
                                                                , sauvegarde="Bdd")
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return cocktail

    @staticmethod
    def find_objet_by_nom(nom_cocktail):
        """ Trouve un objet cocktail dans la base de données
            :param nom_cocktail: nom du cocktail
            :type nom_cocktail: string
            :return cocktail: le cocktail amputé de ses ingrédients
            :rtype Cocktail
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        cocktail = None
        try:
            curseur.execute(
                "SELECT id_cocktail, type, alcool, verre, recette "
                "FROM cocktails WHERE nom_cocktail = %s AND statut = true ;"
                , (nom_cocktail, )
            )
            resultat = curseur.fetchone()
            # Si on a un résultat
            if resultat :
                cocktail = CocktailFactory.generer_cocktail_bdd(id_cocktail=resultat[0]
                                                                , nom_cocktail=nom_cocktail
                                                                , type=resultat[1]
                                                                , alcool=resultat[2]
                                                                , verre=resultat[3]
                                                                , recette=resultat[4]
                                                                , sauvegarde="Bdd")
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        
        return cocktail

    @staticmethod
    def find_by_alcool(alcool):
        """ Trouve les cocktails avec une certaine conteneur en alcool
            :param alcool: la conteneur en alcool
            :type alcool: string
            :return liste: cocktails qui correspondent
            :rtype liste: list <Cocktail>
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        liste = None
        try:
            curseur.execute(
                "SELECT id_cocktail, nom_cocktail, type, verre, recette "
                "FROM cocktails WHERE alcool = %s AND statut = true ;",
                (alcool, )
            )
            resultats = curseur.fetchall()
            # Si on a un résultat
            if resultats :
                liste = []
                for row in resultats :
                    cocktail = CocktailFactory.generer_cocktail_bdd(id_cocktail=row[0]
                                                                    , nom_cocktail=row[1]
                                                                    , type=row[2]
                                                                    , alcool=alcool
                                                                    , verre=row[3]
                                                                    , recette=row[4]
                                                                    , sauvegarde="Bdd")
                    liste += [cocktail]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return liste

    @staticmethod
    def find_by_verre(verre):
        """ Trouve les cocktails avec un certain verre comme contenant
            :param alcool: le contenant
            :type alcool: string
            :return liste: cocktails qui correspondent
            :rtype liste: list <Cocktail>
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        liste = None
        try:
            curseur.execute(
                "SELECT id_cocktail, nom_cocktail, type, alcool, recette "
                "FROM cocktails WHERE verre = %s AND statut = true ;",
                (verre, )
            )
            resultats = curseur.fetchall()
            # Si on a un résultat
            if resultats :
                liste = []
                for row in resultats :
                    cocktail = CocktailFactory.generer_cocktail_bdd(id_cocktail=row[0]
                                                                    , nom_cocktail=row[1]
                                                                    , type=row[2]
                                                                    , alcool=row[3]
                                                                    , verre=verre
                                                                    , recette=row[4]
                                                                    , sauvegarde="Bdd")
                    liste += [cocktail]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return liste

    @staticmethod
    def find_by_type(type):
        """ Trouve les cocktails d'une certaine catégorie
            :param type: la catégorie
            :type type: string
            :return liste: cocktails qui correspondent
            :rtype liste: list <Cocktail>
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        liste = None
        try:
            curseur.execute(
                "SELECT id_cocktail, nom_cocktail, alcool, verre, recette "
                "FROM cocktails WHERE type = %s AND statut = true ;",
                (type, )
            )
            resultats = curseur.fetchall()
            # Si on a un résultat
            if resultats :
                liste = []
                for row in resultats :
                    cocktail = CocktailFactory.generer_cocktail_bdd(id_cocktail=row[0]
                                                                    , nom_cocktail=row[1]
                                                                    , type=type
                                                                    , alcool=row[2]
                                                                    , verre=row[3]
                                                                    , recette=row[4]
                                                                    , sauvegarde="Bdd")
                    liste += [cocktail]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return liste