import psycopg2

from DAO.abstract_dao import AbstractDao
from DAO.pool_connection import PoolConnection

class HistoriqueDao(AbstractDao):

    @staticmethod
    def create(id_user, terme_recherche):
        """ Ajoute une recherche dans notre base de données
            :param id_user: id de l'utilisateur qui a fait la recherche
            :type id_user: int
            :param terme_recherche: le terme de la recherche
            :type terme_recherche: string
            :return: id de la recherche
            :rtype: int
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "INSERT INTO historique (id_user, terme_recherche) "
                " VALUES (%s, %s) RETURNING id_historique ;"
                , (id_user
                   , terme_recherche)
            )
            # On récupère l'id généré
            id_historique = curseur.fetchone()[0]
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return id_historique

    @staticmethod
    def delete(id_historique):
        """ Supprime une recherche de la base
            :param id_historique : la recherche à supprimer
            :type id_historique: int
            :return: si la suppresion a été faite
            :rtype: bool
        """
        deleted = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "DELETE FROM historique WHERE id_historique = %s ;"
                , (id_historique, )
            )
            # On verifie s'il y a eu des supressions
            if curseur.rowcount > 0:
                deleted = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return deleted

    @staticmethod
    def find_by_id_user(id_user):
        """ Trouve les recherches d'un l'utilisateur
            :param id_user : id de l'utilisateur
            :type id_user : int
            :return historique de l'utilisateur
            :rtype dict
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_historique, terme_recherche "
                "FROM historique WHERE id_user = %s ;"
                , (id_user, )
            )
            resultats = curseur.fetchall()  
            historique = None
            if resultats :
                historique = {}
                i = 0
                for row in resultats :
                    historique[i] = {}
                    historique[i]["id_historique"] = row[0]
                    historique[i]["id_user"] = id_user
                    historique[i]["terme_recherche"] = row[1]
                    i += 1
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return historique

    @staticmethod
    def get_all():
        """ Charge tout l'historique de recherche de l'application
            :param
            :type
            :return tout l'historique
            :rtype dict
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_historique, id_user, terme_recherche "
                "FROM historique ORDER BY id_user ;"
            )
            resultats = curseur.fetchall()  
            historique = None
            if resultats :
                historique = {}
                i = 0
                for row in resultats :
                    historique[i] = {}
                    historique[i]["id_historique"] = row[0]
                    historique[i]["id_user"] = row[1]
                    historique[i]["terme_recherche"] = row[2]
                    i += 1
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
            
        return historique