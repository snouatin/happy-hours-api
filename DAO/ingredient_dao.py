import psycopg2

from DAO.abstract_dao import AbstractDao
from DAO.pool_connection import PoolConnection
from Business_objects.ingredient import Ingredient
from Factory.ingredient_factory import IngredientFactory

class IngredientDao(AbstractDao):

    @staticmethod
    def create(nom_ingredient):
        """ Ajoute un ingredient dans notre base de données
            :param ingredient: l'ingredient a ajouter
            :type ingredient: Ingredient
            :return: l'ingredient mis à jour de son id
            :rtype: Ingredient
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "INSERT INTO ingredients (nom_ingredient)"
                "VALUES (%s) RETURNING id_ingredient ;"
                , (nom_ingredient, )
            )
            # On récupère l'id généré
            id_ingredient = curseur.fetchone()[0]
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
            
        return id_ingredient

    @staticmethod
    def delete(id_ingredient):
        """ Supprime un ingredient de la base
            :param id_ingredient : id de l'ingredient a supprimer
            :type id_ingredient: int
            :return: si la suppresion a été faite
            :rtype: bool
        """
        deleted = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "DELETE FROM ingredients WHERE id_ingredient = %s ;"
                , (id_ingredient, ))

            # On verifie s'il y a eu des supressions
            if curseur.rowcount > 0:
                deleted = True

            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return deleted

    @staticmethod
    def completion_ingredients(cocktail):
        """ Complète les ingrédients d'un cocktail en ajoutant les noms
            :param coktail : un cocktail
            :type coktail : Cocktail
            :return le même cocktail mis à jour des noms des ingrédients
            :rtype Cocktail
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            for ingredient in cocktail.ingredients :
                curseur.execute(
                    "SELECT nom_ingredient "
                    "FROM ingredients WHERE id_ingredient = %s ;"
                    , (ingredient.id, )
                )
                resultat = curseur.fetchone()
                # Si on a un résultat
                if resultat:
                    ingredient.nom = resultat[0]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return cocktail

    @staticmethod
    def find_by_id_ingredient(id_ingredient):
        """ Trouve le nom d'un ingredient dont on connait son id
            :param id_ingredient : id de l'ingredient qu'on cherche
            :type id_ingredient : string
            :return le nom de l'ingredient 
            :rtype string
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT nom_ingredient "
                "FROM ingredients WHERE id_ingredient = %s ;"
                , (id_ingredient, )
            )
            resultat = curseur.fetchone()
            nom_ingredient = None
            # Si on a un résultat
            if resultat:
                nom_ingredient = resultat[0]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return nom_ingredient

    @staticmethod
    def find_by_nom(nom_ingredient):
        """ Trouve le nom d'un ingredient dont on connait son id
            :param id_ingredient : id de l'ingredient qu'on cherche
            :type id_ingredient : string
            :return le nom de l'ingredient 
            :rtype string
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_ingredient "
                "FROM ingredients WHERE nom_ingredient = %s ;"
                , (nom_ingredient, )
            )
            resultat = curseur.fetchone()
            id_ingredient = None
            # Si on a un résultat
            if resultat:
                id_ingredient = resultat[0]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        return id_ingredient

    @staticmethod
    def find_by_liste_id_ingredient(liste_id_ingredient):
        """ Trouve le nom d'ingrédients
            :param id_coktail : id de l'ingrédient
            :type id_coktail : int
            :return les noms
            :rtype list
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            liste_nom_ingredient = []
            for id_ingredient in liste_id_ingredient :
                curseur.execute(
                    "SELECT nom_ingredient "
                    "FROM ingredients WHERE id_ingredient = %s ;"
                    , (id_ingredient, )
                )
                resultat = curseur.fetchone()
                nom_ingredient = None
                # Si on a un résultat
                if resultat:
                    nom_ingredient = resultat[0]
                    liste_nom_ingredient += [nom_ingredient]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return liste_nom_ingredient