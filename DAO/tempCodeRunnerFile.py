from psycopg2.extras import RealDictCursor

import Configuration.properties as properties

import psycopg2
from psycopg2 import pool

class PoolConnection:
    """ Cette classe va gérer la connexion à la base de données
    """

    __instance = None

    @staticmethod
    def getInstance():
        """ C'est la méthode que l'on va utiliser si l'on veut obtenir l'instance
            de de ReservoirConnexion
            :return: le singleton ReservoirConnexion
            :rtype: PoolConnection
        """
        if PoolConnection.__instance is None:
            PoolConnection()
        return PoolConnection.__instance


    @staticmethod
    def getConnexion():
        """ Méthode qui retourne une connexion utilisable
            :return: une connexion à la base
        """
        return PoolConnection.getInstance().getconn()

    @staticmethod
    def closeConnexions():
        """ Ferme toutes les connexions ouvertes
            :return: si les connexions ont pu être fermées
            :rtype: bool
        """
        try :
            PoolConnection.getInstance().closeall
            closed = True
        except Exception :
            print("Problème lors de la fermeture")
            closed = False
        return closed

    @staticmethod
    def putBackConnexion(connection):
        PoolConnection.getInstance().putconn(connection)

    def __init__(self):
        """ Constructeur de la classe
        """
        if PoolConnection.__instance != None:
            raise Exception("Cette classe est un singleton" 
                            "Utiliser la méthode getInstance()")
        else:
            PoolConnection.__instance = psycopg2.pool.SimpleConnectionPool(1, 2,
                                                                           host=properties.host,
                                                                           port=properties.port,
                                                                           database=properties.database,
                                                                           user=properties.user,
                                                                           password=properties.password)