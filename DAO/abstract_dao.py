from abc import ABC

class AbstractDao(ABC):
    """ Classe abstraite dont les test_dao doivent hériter. Permet d'avoir les noms des
        méthodes de base des test_dao identique. À la différence d'autres classes
        abstraites ici on n'attend pas que les classe implémentent
        obligatoirement les méthodes. On essaye seulement de normaliser les
        noms pour une meilleure lisibilité du code
    """

    @staticmethod
    def create(business_object):
        """ Insère une ligne en base avec l'objet en paramètre. 
            Retourne l'objet mis à jour avec son id de la base.
        """
        return NotImplementedError

    @staticmethod
    def delete(business_object):
        """ Supprime la ligne en base représentant l'objet en paramètre
            :return si une supression à eu lieu
            :rtype bool
        """
        return NotImplementedError

    @staticmethod
    def update(business_object):
        """ Met à jour la ligne en base de donnée associé 
            à l'objet métier en paramètre
        """

        return NotImplementedError

