import psycopg2

from DAO.abstract_dao import AbstractDao
from DAO.pool_connection import PoolConnection
from Business_objects.avis import Avis
from Factory.avis_factory import AvisFactory

class AvisApiDao():

    @staticmethod
    def create(avis):
        """ Ajoute un avis dans notre base de donnees
            :param avis: avis qu'on veut ajouter
            :type avis: Avis
        """
        created = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On cree le cocktail dans la table des cocktails
            curseur.execute(
                "INSERT INTO avis_api (id_user, id_cocktail_api, note, commentaire) "
                "VALUES (%s, %s, %s, %s) ;"
                , (avis.id_user
                   , avis.id_cocktail
                   , avis.note
                   , avis.commentaire)
            )
            if curseur.rowcount > 0:
                created = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # La transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)
        
        return created

    @staticmethod
    def update(avis):
        """ Met a jour un avis dans la base de donnees
            :param avis: avis qu'on met a jour
            :type avis: Avis
            :return: si la mise à jour a ete faite
            :rtype: bool
        """
        updated = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                    "UPDATE avis_api "
                    "SET note = %s, commentaire = %s "
                    "WHERE id_user = %s AND id_cocktail_api = %s ;"
                    , (avis.note
                        , avis.commentaire
                        , avis.id_user
                        , avis.id_cocktail) )
            if curseur.rowcount > 0:
                updated = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return updated

    @staticmethod
    def delete(avis):
        """ Supprime un avis de la base
            :param avis : avis à supprimer
            :type avis: Avis
            :return: si la suppresion a été faite
            :rtype: bool
        """
        deleted = False
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            # On envoie au serveur la requête SQL
            curseur.execute(
                "DELETE FROM avis_api WHERE id_user = %s AND id_cocktail_api = %s ;"
                , (avis.id_user
                    , avis.id_cocktail) )
            # On verifie s'il y a eu des supressions
            if curseur.rowcount > 0:
                deleted = True
            # On enregistre la transaction en base
            connexion.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connexion.rollback()
            raise error
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return deleted

    @staticmethod
    def find_by_id_user(id_user):
        """ Trouve les avis d'un utilisateur
            :param id_user : id de l'utilisateur
            :type id_user : int
            :return les avis
            :rtype list <Avis>
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_cocktail_api, note, commentaire "
                "FROM avis_api WHERE id_user = %s ;"
                , (id_user, )
            )
            resultats = curseur.fetchall()
            # Si on a un résultat
            liste_avis = None
            if resultats:
                liste_avis = []
                for row in resultats:
                    avis = AvisFactory.generer_avis(id_user=id_user
                                                        , id_cocktail=row[0]
                                                        , note=row[1]
                                                        , commentaire=row[2])
                    liste_avis += [avis]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return liste_avis

    @staticmethod
    def find_by_id_cocktail(id_cocktail):
        """ Trouve les avis d'un cocktail
            :param id_cocktail : id du cocktail
            :type id_cocktail : int
            :return les avis
            :rtype list <Avis>
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT id_user, note, commentaire "
                "FROM avis_api WHERE id_cocktail_api = %s ;"
                , (id_cocktail, )
            )
            resultats = curseur.fetchall()
            # Si on a un résultat
            liste_avis = None
            if resultats:
                liste_avis = []
                for row in resultats:
                    avis = AvisFactory.generer_avis(id_user=row[0]
                                                        , id_cocktail=id_cocktail
                                                        , note=row[1]
                                                        , commentaire=row[2])
                    liste_avis += [avis]
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return liste_avis

    @staticmethod
    def find_by_id_user_cocktail(id_user, id_cocktail):
        """ Trouve l'avis d'un utilisateur à un cocktail
            :param id_user : id de l'utilisateur
            :type id_user : int
            :param id_cocktail : id du cocktail
            :type id_cocktail : int
            :return les avis
            :rtype list <Avis>
        """
        connexion = PoolConnection.getConnexion()
        curseur = connexion.cursor()
        try:
            curseur.execute(
                "SELECT note, commentaire "
                "FROM avis_api "
                "WHERE id_user = %s AND id_cocktail_api = %s ;"
                , (id_user
                    , id_cocktail)
            )
            resultat = curseur.fetchone()
            # Si on a un résultat
            avis = None
            if resultat:
                avis = AvisFactory.generer_avis(id_user=id_user
                                                    , id_cocktail=id_cocktail
                                                    , note=resultat[0]
                                                    , commentaire=resultat[1])
        finally:
            curseur.close()
            PoolConnection.putBackConnexion(connexion)

        return avis