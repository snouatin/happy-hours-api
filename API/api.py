import requests
import json
from Factory.ingredient_factory import IngredientFactory
from Factory.cocktail_factory import CocktailFactory

class Api():
    """ Méthodes qui vont permettre de requêter l'API
    """

    @staticmethod
    def find_objet_by_nom(nom_cocktail):
        """ Permet de trouver sur l'API un cocktail particulier
            :param nom_cocktail: nom du cocktail
            :type nom_cocktail: string
            :return cocktail: le cocktail en question
            :rtype cocktail: Cocktail
        """
        response = requests.get("https://www.thecocktaildb.com/api/json/v1/1/search.php?s={}".format(nom_cocktail))
        response = response.json()["drinks"]
        cocktail = None
        if response :
            response = response[0]
            ingredients = []
            i = 1
            while i < 16 and response["strIngredient"+str(i)] :
                ingredient = IngredientFactory.generer_ingredient(id_ingredient=None
                                                                    , nom_ingredient=response["strIngredient"+str(i)]
                                                                    , mesure=response["strMeasure"+str(i)])
                ingredients += [ingredient]
                i += 1
            cocktail = CocktailFactory.generer_cocktail_api(id_cocktail=int(response["idDrink"])
                                                            , nom_cocktail=response["strDrink"]
                                                            , type=response["strCategory"]
                                                            , alcool=response["strAlcoholic"]
                                                            , verre=response["strGlass"]
                                                            , ingredients=ingredients
                                                            , recette=response["strInstructions"]
                                                            , URL=response["strDrinkThumb"]
                                                            , sauvegarde="Api")
        return cocktail

    @staticmethod
    def find_objet(id_cocktail):
        """ Permet de trouver sur l'API un cocktail particulier
            :param id_cocktail: id du cocktail
            :type id_cocktail: int
            :return cocktail: le cocktail en question
            :rtype cocktail: Cocktail
        """
        response = requests.get("https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i={}".format(id_cocktail))
        response = response.json()["drinks"]
        cocktail = None
        if response :
            response = response[0]
            ingredients = []
            i = 1
            while i < 16 and response["strIngredient"+str(i)] :
                ingredient = IngredientFactory.generer_ingredient(id_ingredient=None
                                                                    , nom_ingredient=response["strIngredient"+str(i)]
                                                                    , mesure=response["strMeasure"+str(i)])
                ingredients += [ingredient]
                i += 1
            cocktail = CocktailFactory.generer_cocktail_api(id_cocktail=int(response["idDrink"])
                                                            , nom_cocktail=response["strDrink"]
                                                            , type=response["strCategory"]
                                                            , alcool=response["strAlcoholic"]
                                                            , verre=response["strGlass"]
                                                            , ingredients=ingredients
                                                            , recette=response["strInstructions"]
                                                            , URL=response["strDrinkThumb"]
                                                            , sauvegarde="Api")
        return cocktail

    @staticmethod
    def find_by_nom_ingredient(nom_ingredient):
        """ Permet de trouver sur l'API les cocktails contenant un ingrédient en particulier
            :param nom_ingredient: nom de l'ingrédient qu'on veut dans tous les cocktails
            :type nom_ingredient: string
            :return cocktails: les cocktail en question
            :rtype cocktails: list <Cocktail>
        """
        response = requests.get("https://www.thecocktaildb.com/api/json/v1/1/filter.php?i={}".format(nom_ingredient))
        response = response.json()["drinks"]
        cocktails = None
        if response :
            cocktails = []
            for i in range(len(response)) :
                cocktail = Api.find_objet_by_nom(nom_cocktail=response[i]["strDrink"])
                cocktails += [cocktail]

        return cocktails

    @staticmethod
    def find_by_alcool(alcool):
        """ Permet de trouver sur l'API les cocktails avec une certaine conteneur en alcool
            :param alcool: conteneur en alcool du cocktail
            :type alcool: string
            :return cocktails: les cocktail en question
            :rtype cocktails: list <Cocktail>
        """
        response = requests.get("https://www.thecocktaildb.com/api/json/v1/1/filter.php?a={}".format(alcool))
        response = response.json()["drinks"]
        cocktails = None
        if response :
            cocktails = []
            for i in range(len(response)) :
                cocktail = Api.find_objet_by_nom(nom_cocktail=response[i]["strDrink"])
                cocktails += [cocktail]

        return cocktails

    @staticmethod
    def find_by_verre(verre):
        """ Permet de trouver sur l'API les cocktails avec un certain contenenant
            :param verre: contenenant du cocktail
            :type verre: string
            :return cocktails: les cocktail en question
            :rtype cocktails: list <Cocktail>
        """
        response = requests.get("https://www.thecocktaildb.com/api/json/v1/1/filter.php?g={}".format(verre))
        response = response.json()["drinks"]
        cocktails = None
        if response :
            cocktails = []
            for i in range(len(response)) :
                cocktail = Api.find_objet_by_nom(nom_cocktail=response[i]["strDrink"])
                cocktails += [cocktail]

        return cocktails

    @staticmethod
    def find_by_type(type):
        """ Permet de trouver sur l'API les cocktails d'une certaine catégorie
            :param type: catégorie du cocktail
            :type type: string
            :return cocktails: les cocktail en question
            :rtype cocktails: list <Cocktail>
        """
        response = requests.get("https://www.thecocktaildb.com/api/json/v1/1/filter.php?c={}".format(type))
        response = response.json()["drinks"]
        cocktails = None
        if response :
            cocktails = []
            for i in range(len(response)) :
                cocktail = Api.find_objet_by_nom(nom_cocktail=response[i]["strDrink"])
                cocktails += [cocktail]

        return cocktails

    @staticmethod
    def find_tous_verres():
        """ Permet de trouver tous les types de contenants
        """
        liste = []
        response = requests.get("https://www.thecocktaildb.com/api/json/v1/1/list.php?g=list")
        response = response.json()["drinks"]
        for i in range(len(response)):
            liste += [response[i]["strGlass"]]
        return liste

    @staticmethod
    def find_tous_alcools():
        """ Permet de trouver toutes les conteneurs en alcool
        """
        liste = []
        response = requests.get("https://www.thecocktaildb.com/api/json/v1/1/list.php?a=list")
        response = response.json()["drinks"]
        for i in range(len(response)):
            liste += [response[i]["strAlcoholic"]]
        return liste

    @staticmethod
    def find_tous_types():
        """ Permet de trouver toutes les types de cocktails
        """
        liste = []
        response = requests.get("https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list")
        response = response.json()["drinks"]
        for i in range(len(response)):
            liste += [response[i]["strCategory"]]
        return liste