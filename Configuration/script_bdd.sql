DROP SEQUENCE IF EXISTS  id_utilisateur CASCADE;
CREATE SEQUENCE id_utilisateur;

DROP TYPE IF EXISTS type_utilisateur CASCADE;
CREATE TYPE type_utilisateur AS ENUM ('Visiteur', 'Regulier', 'Administrateur');

DROP TABLE IF EXISTS utilisateurs CASCADE;
CREATE TABLE utilisateurs (
    id_user integer NOT NULL DEFAULT nextval('id_utilisateur'),
    type_user type_utilisateur,
    identifiant text COLLATE pg_catalog."default",
    pass_word text COLLATE pg_catalog."default",
    nom text COLLATE pg_catalog."default",
    prenom text COLLATE pg_catalog."default",
    email text COLLATE pg_catalog."default",
    CONSTRAINT user_pkey PRIMARY KEY (id_user)
);

DROP SEQUENCE IF EXISTS  id_history CASCADE;
CREATE SEQUENCE id_history;

DROP TABLE IF EXISTS historique CASCADE;
CREATE TABLE historique (
    id_historique integer NOT NULL DEFAULT nextval('id_history'),
    id_user integer,
    terme_recherche text COLLATE pg_catalog."default",
    CONSTRAINT historique_pkey PRIMARY KEY (id_historique),
    CONSTRAINT historique_fkey FOREIGN KEY (id_user) REFERENCES utilisateurs(id_user) ON DELETE CASCADE
);

DROP SEQUENCE IF EXISTS  id_mix CASCADE;
CREATE SEQUENCE id_mix;

DROP TABLE IF EXISTS cocktails CASCADE;
CREATE TABLE cocktails (
    id_cocktail integer NOT NULL DEFAULT nextval('id_mix'),
    nom_cocktail text COLLATE pg_catalog."default",
    type text COLLATE pg_catalog."default",
    alcool text COLLATE pg_catalog."default",
    verre text COLLATE pg_catalog."default",
    recette text COLLATE pg_catalog."default",
    statut boolean,
    CONSTRAINT cocktail_pkey PRIMARY KEY (id_cocktail)
);

DROP SEQUENCE IF EXISTS  id_ing CASCADE;
CREATE SEQUENCE id_ing;

DROP TABLE IF EXISTS ingredients CASCADE;
CREATE TABLE ingredients (
    id_ingredient integer NOT NULL DEFAULT nextval('id_ing'),
    nom_ingredient text COLLATE pg_catalog."default",
    CONSTRAINT ingredients_pkey PRIMARY KEY (id_ingredient)
);

DROP TABLE IF EXISTS compositions CASCADE;
CREATE TABLE compositions (
    id_cocktail integer,
    id_ingredient integer,
    mesure text COLLATE pg_catalog."default",
    CONSTRAINT compositions_pkey PRIMARY KEY (id_cocktail, id_ingredient),
    CONSTRAINT cocktail_composition_fkey FOREIGN KEY (id_cocktail) REFERENCES cocktails(id_cocktail) ON DELETE CASCADE,
    CONSTRAINT ingredient_composition_fkey FOREIGN KEY (id_ingredient) REFERENCES ingredients(id_ingredient) ON DELETE CASCADE
);

DROP TABLE IF EXISTS avis CASCADE;
CREATE TABLE avis (
    id_user integer,
    id_cocktail integer,
    note float,
    commentaire text COLLATE pg_catalog."default",
    CONSTRAINT avis_pkey PRIMARY KEY (id_user, id_cocktail),
    CONSTRAINT user_avis_fkey FOREIGN KEY (id_user) REFERENCES utilisateurs(id_user) ON DELETE CASCADE,
    CONSTRAINT cocktail_avis_fkey FOREIGN KEY (id_cocktail) REFERENCES cocktails(id_cocktail) ON DELETE CASCADE
);

DROP TABLE IF EXISTS avis_api CASCADE;
CREATE TABLE avis_api (
    id_user integer,
    id_cocktail_api integer,
    note float,
    commentaire text COLLATE pg_catalog."default",
    CONSTRAINT avis_api_pkey PRIMARY KEY (id_user, id_cocktail_api),
    CONSTRAINT user_avis_api_fkey FOREIGN KEY (id_user) REFERENCES utilisateurs(id_user) ON DELETE CASCADE
);

DROP TABLE IF EXISTS favoris CASCADE;
CREATE TABLE favoris (
    id_user integer,
    id_cocktail integer,
    CONSTRAINT favoris_pkey PRIMARY KEY (id_user, id_cocktail),
    CONSTRAINT user_favori_fkey FOREIGN KEY (id_user) REFERENCES utilisateurs(id_user) ON DELETE CASCADE,
    CONSTRAINT cocktail_favori_fkey FOREIGN KEY (id_cocktail) REFERENCES cocktails(id_cocktail) ON DELETE CASCADE
);

DROP TABLE IF EXISTS favoris_api CASCADE;
CREATE TABLE favoris_api (
    id_user integer,
    id_cocktail_api integer,
    CONSTRAINT favoris_api_pkey PRIMARY KEY (id_user, id_cocktail_api),
    CONSTRAINT user_favori_api_fkey FOREIGN KEY (id_user) REFERENCES utilisateurs(id_user) ON DELETE CASCADE
);

INSERT INTO utilisateurs(type_user, identifiant, pass_word, nom, prenom, email) 
VALUES ('Administrateur', 'artfav', 'f6dd224cc34fabf788c4e96a86e1176252bba8316293e9ba84fa11b45db64c2d', 'Favereaux', 'Arthur', 'pro.arthurfavereaux@gmail.com') ;

INSERT INTO utilisateurs(type_user, identifiant, pass_word, nom, prenom, email) 
VALUES ('Administrateur', 'leomoq', 'f10643c55056337ab13e38c9d83daf338186a39c9e298712d042375702b999d2', 'Moquay', 'Léo', 'leo.moquay@gmail.com') ;

INSERT INTO utilisateurs(type_user, identifiant, pass_word, nom, prenom, email)
VALUES ('Administrateur', 'snouatin', '2c9cc3fe2299090c53702408d48da9df47008839c87850a389a1c7d215136789', 'Nouatin', 'Stève', 'stevenouatin@gmail.com') ;


INSERT INTO cocktails(nom_cocktail, type, alcool, verre, recette, statut) 
VALUES ('Soupe'
        , 'Cocktail'
        , 'Alcoholic'
        , 'Cocktail Glass'
        , 'Ajouter la vodka, puis la grendadine et enfin le jus. Mélanger et déguster.'
        , True) ;

INSERT INTO ingredients(nom_ingredient) 
VALUES ('Vodka') ;

INSERT INTO ingredients(nom_ingredient) 
VALUES ('Grenadine') ;

INSERT INTO ingredients(nom_ingredient) 
VALUES ('Jus Multifruits') ;

INSERT INTO compositions(id_cocktail, id_ingredient, mesure) 
VALUES (1, 1, '1 oz') ;

INSERT INTO compositions(id_cocktail, id_ingredient, mesure) 
VALUES (1, 2, '1 oz') ;

INSERT INTO compositions(id_cocktail, id_ingredient, mesure) 
VALUES (1, 3, '5 oz') ;

INSERT INTO historique(id_user, terme_recherche)
VALUES (1, 'Grenadine') ;