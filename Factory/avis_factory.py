from Business_objects.avis import Avis

class AvisFactory():

    """ Classe qui implemente le patern factory
        Elle nous permet d'instancier des avis sans que l'on ait à savoir comment elle fait
    """

    @staticmethod
    def generer_avis(id_user, id_cocktail, note, commentaire):
        """ Va creer un ingredient a partir des parametres passes
            :param id_user: id de l'utilisateur
            :type id_user: int
            :param id_cocktail: id du cocktail
            :type id_cocktail: int
            :param note: note du cocktail
            :type note: float
            :param commentaire: commentaire du cocktail
            :type commentaire: string
            :return: l'avis cree
            :rtype: Avis
        """
        avis = Avis(id_user=id_user
                        , id_cocktail=id_cocktail
                        , note=note
                        , commentaire=commentaire)

        return avis