from Business_objects.utilisateur import Utilisateur

class UtilisateurFactory():

    """ Classe qui implemente le patern factory
        Elle nous permet d'instancier des utilisateurs sans que l'on ait à savoir comment elle fait
    """

    @staticmethod
    def generer_utilisateur(id_user, TYPE_BASE_DE_DONNEE, identifiant, password, nom, prenom, email):
        """ Va creer un utilisateur a partir des parametres passes
            :param id_user: l'id du personnage
            :type id_user: int
            :param TYPE_BASE_DE_DONNEE: type de l'utilisateur
            :type TYPE_BASE_DE_DONNEE: string
            :param identifiant: l'identifiant de l'utilisateur
            :type identifiant: string
            :param password: le mot de passe de l'utilisateur
            :type password: string
            :param nom: le nom de l'utilisateur
            :type nom: string
            :param prenom: le prenom de l'utilisateur
            :type prenom: string
            :param email: l'email de l'utilisateur
            :type email: string
            :return: l'utilisateur cree
            :rtype: Utilisateur
        """
        utilisateur = None
        utilisateur = Utilisateur(id_user=id_user
                                  , TYPE_BASE_DE_DONNEE=TYPE_BASE_DE_DONNEE
                                  , identifiant=identifiant
                                  , password=password
                                  , nom=nom
                                  , prenom=prenom
                                  , email=email)

        return utilisateur