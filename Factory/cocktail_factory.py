from Business_objects.cocktail import Cocktail

class CocktailFactory():

    """ Classe qui implemente le patern factory
        Elle nous permet d'instancier des cocktails sans que l'on ait à savoir comment elle fait
    """

    @staticmethod
    def generer_cocktail_bdd(id_cocktail, nom_cocktail, type, alcool, verre, recette, sauvegarde):
        """ Crée un cocktail à partir d'une recherche sur la base de données
            :param id_cocktail: id du cocktail
            :type id_cocktail: int
            :param nom_cocktail: nom du cocktail
            :type nom_cocktail: string
            :param type: type du cocktail
            :type type: string
            :param alcool: conteneur en alcool
            :type alcool: string
            :param verre: verre du cocktail
            :type verre: string
            :param recette: recette du cocktail
            :type recette: string
            :param sauvegarde: lieu de sauvegarde du cocktail
            :type sauvegarde: string
            :return cocktail: le cocktail
            :rtype cocktail: Cocktail
        """
        cocktail = Cocktail(id_cocktail=id_cocktail
                            , nom=nom_cocktail
                            , type=type
                            , alcool=alcool
                            , verre=verre
                            , ingredients = None
                            , recette=recette
                            , URL=None
                            , sauvegarde=sauvegarde)

        return cocktail

    @staticmethod
    def generer_cocktail_api(id_cocktail, nom_cocktail, type, alcool, verre, ingredients, recette, URL, sauvegarde):
        """ Crée un cocktail à partir d'une recherche sur l'api
            :param id_cocktail: id du cocktail
            :type id_cocktail: int
            :param nom_cocktail: nom du cocktail
            :type nom_cocktail: string
            :param type: type du cocktail
            :type type: string
            :param alcool: conteneur en alcool
            :type alcool: string
            :param verre: verre du cocktail
            :type verre: string
            :param ingredients: ingredients du cocktail
            :type ingredients: list <Ingredient>
            :param recette: recette du cocktail
            :type recette: string
            :param URL: lien de l'image du cocktail
            :type URL: string
            :param sauvegarde: lieu de sauvegarde du cocktail
            :type sauvegarde: string
            :return cocktail: le cocktail
            :rtype cocktail: Cocktail
        """
        cocktail = Cocktail(id_cocktail=id_cocktail
                            , nom=nom_cocktail
                            , type=type
                            , alcool=alcool
                            , verre=verre
                            , ingredients = ingredients
                            , recette=recette
                            , URL=URL
                            , sauvegarde=sauvegarde)

        return cocktail