from Business_objects.ingredient import Ingredient

class IngredientFactory():

    """ Classe qui implemente le patern factory
        Elle nous permet d'instancier des ingredients sans que l'on ait à savoir comment elle fait
    """

    @staticmethod
    def generer_ingredient(id_ingredient, nom_ingredient, mesure):
        """ Crée un ingredient à partir de l'id du cocktail et de son id
            :param id_ingredient: id de l'ingredient
            :type id_ingredient: int
            :param nom_ingredient: nom de l'ingredient
            :type nom_ingredient: string
            :param unite: unite de l'ingredient
            :type unite: string
            :param quantite: quantite de l'ingredient
            :type quantite: float
            :return: l'ingredient créé
            :rtype: Ingredient
        """
        ingredient = Ingredient(id_ingredient=id_ingredient
                                  , nom=nom_ingredient
                                  , mesure=mesure)

        return ingredient
