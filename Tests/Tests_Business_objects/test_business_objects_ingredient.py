from unittest import TestCase

from Business_objects.ingredient import Ingredient

class TestBusinessObjectsIngredient(TestCase):

    def test_creation_ingredient_id(self):
        expected_output = 1
        ingredient = Ingredient(id_ingredient=expected_output
                                , nom=None
                                , mesure=None)
        actual_output = ingredient.id
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_ingredient_nom(self):
        expected_output = "test"
        ingredient = Ingredient(id_ingredient=None
                                , nom=expected_output
                                , mesure=None)
        actual_output = ingredient.nom
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_ingredient_mesure(self):
        expected_output = "test"
        ingredient = Ingredient(id_ingredient=None
                                , nom=None
                                , mesure=expected_output)
        actual_output = ingredient.mesure
        self.assertEquals(first=expected_output, second=actual_output)