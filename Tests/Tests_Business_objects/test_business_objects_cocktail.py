from unittest import TestCase
from Business_objects.cocktail import Cocktail

class TestBusinessObjectsCocktail(TestCase):

    def test_creation_cocktail_nom(self):
        expected_output = "test"
        cocktail = Cocktail(id_cocktail=None
                                , nom=expected_output
                                , type = None
                                , alcool=None
                                , verre=None
                                , ingredients=None
                                , recette=None
                                , URL=None
                                , sauvegarde=None)
        actual_output = cocktail.nom
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_cocktail_type(self):
        expected_output = "test"
        cocktail = Cocktail(id_cocktail=None
                                , nom=None
                                , type=expected_output
                                , alcool=None
                                , verre=None
                                , ingredients=None
                                , recette=None
                                , URL=None
                                , sauvegarde=None)
        actual_output = cocktail.type
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_cocktail_alcool(self):
        expected_output = "test"
        cocktail = Cocktail(id_cocktail=None
                                , nom=None
                                , type=None
                                , alcool=expected_output
                                , verre=None
                                , ingredients=None
                                , recette=None
                                , URL=None
                                , sauvegarde=None)
        actual_output = cocktail.alcool
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_cocktail_verre(self):
        expected_output = "test"
        cocktail = Cocktail(id_cocktail=None
                                , nom=None
                                , type=None
                                , alcool=None
                                , verre=expected_output
                                , ingredients=None
                                , recette=None
                                , URL=None
                                , sauvegarde=None)
        actual_output = cocktail.verre
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_cocktail_ingredients(self):
        expected_output = "test"
        cocktail = Cocktail(id_cocktail=None
                                , nom=None
                                , type=None
                                , alcool=None
                                , verre=None
                                , ingredients=expected_output
                                , recette=None
                                , URL=None
                                , sauvegarde=None)
        actual_output = cocktail.ingredients
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_cocktail_recette(self):
        expected_output = "test"
        cocktail = Cocktail(id_cocktail=None
                                , nom=None
                                , type=None
                                , alcool=None
                                , verre=None
                                , ingredients=None
                                , recette=expected_output
                                , URL=None
                                , sauvegarde=None)
        actual_output = cocktail.recette
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_cocktail_url(self):
        expected_output = "test"
        cocktail = Cocktail(id_cocktail=None
                                , nom=None
                                , type=None
                                , alcool=None
                                , verre=None
                                , ingredients=None
                                , recette=None
                                , URL=expected_output
                                , sauvegarde=None)
        actual_output = cocktail.URL
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_cocktail_sauvegarde(self):
        expected_output = "test"
        cocktail = Cocktail(id_cocktail=None
                                , nom=None
                                , type=None
                                , alcool=None
                                , verre=None
                                , ingredients=None
                                , recette=None
                                , URL=None
                                , sauvegarde=expected_output)
        actual_output = cocktail.sauvegarde
        self.assertEquals(first=expected_output, second=actual_output)