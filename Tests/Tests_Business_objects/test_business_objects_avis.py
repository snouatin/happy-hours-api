from unittest import TestCase

from Business_objects.avis import Avis

class TestBusinessObjectsAvis(TestCase):

    def test_creation_avis_id_user(self):
        expected_output = 1
        avis = Avis(id_user=expected_output
                        , id_cocktail=None
                        , note=None
                        , commentaire=None)
        actual_output = avis.id_user
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_avis_id_cocktail(self):
        expected_output = 1
        avis = Avis(id_user=None
                        , id_cocktail=expected_output
                        , note=None
                        , commentaire=None)
        actual_output = avis.id_cocktail
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_avis_note(self):
        expected_output = 1
        avis = Avis(id_user=None
                        , id_cocktail=None
                        , note=expected_output
                        , commentaire=None)
        actual_output = avis.note
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_avis_note(self):
        expected_output = "test"
        avis = Avis(id_user=None
                        , id_cocktail=None
                        , note=None
                        , commentaire=expected_output)
        actual_output = avis.commentaire
        self.assertEquals(first=expected_output, second=actual_output)