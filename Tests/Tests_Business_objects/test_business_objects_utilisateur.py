from unittest import TestCase
from Business_objects.utilisateur import Utilisateur

class TestBusinessObjectsUtilisateur(TestCase):

    def test_creation_utilisateur_type(self):
        expected_output = "test"
        utilisateur = Utilisateur(id_user=None
                                    , TYPE_BASE_DE_DONNEE=expected_output
                                    , identifiant=None
                                    , password=None
                                    , nom=None
                                    , prenom=None
                                    , email=None)
        actual_output = utilisateur.type
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_utilisateur_identifiant(self):
        expected_output = "test"
        utilisateur = Utilisateur(id_user=None
                                    , TYPE_BASE_DE_DONNEE=None
                                    , identifiant=expected_output
                                    , password=None
                                    , nom=None
                                    , prenom=None
                                    , email=None)
        actual_output = utilisateur.identifiant
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_utilisateur_password(self):
        expected_output = "test"
        utilisateur = Utilisateur(id_user=None
                                    , TYPE_BASE_DE_DONNEE=None
                                    , identifiant=None
                                    , password=expected_output
                                    , nom=None
                                    , prenom=None
                                    , email=None)
        actual_output = utilisateur.password
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_utilisateur_nom(self):
        expected_output = "test"
        utilisateur = Utilisateur(id_user=None
                                    , TYPE_BASE_DE_DONNEE=None
                                    , identifiant=None
                                    , password=None
                                    , nom=expected_output
                                    , prenom=None
                                    , email=None)
        actual_output = utilisateur.nom
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_utilisateur_prenom(self):
        expected_output = "test"
        utilisateur = Utilisateur(id_user=None
                                    , TYPE_BASE_DE_DONNEE=None
                                    , identifiant=None
                                    , password=None
                                    , nom=None
                                    , prenom=expected_output
                                    , email=None)
        actual_output = utilisateur.prenom
        self.assertEquals(first=expected_output, second=actual_output)

    def test_creation_utilisateur_email(self):
        expected_output = "test"
        utilisateur = Utilisateur(id_user=None
                                    , TYPE_BASE_DE_DONNEE=None
                                    , identifiant=None
                                    , password=None
                                    , nom=None
                                    , prenom=None
                                    , email=expected_output)
        actual_output = utilisateur.email
        self.assertEquals(first=expected_output, second=actual_output)