from unittest import TestCase

from DAO.pool_connection import PoolConnection

class TestPoolConnection(TestCase):

    def test_getInstance(self):
        # GIVEN
        reservoir_connexion = PoolConnection.getInstance()
        # THEN
        self.assertIsNotNone(reservoir_connexion)

    def test_getConnexion(self):
        # GIVEN
        connexion = PoolConnection.getConnexion()
        # THEN
        self.assertIsNotNone(connexion)