from unittest import TestCase

from DAO.ingredient_dao import IngredientDao
from DAO.cocktail_dao import CocktailDao
from DAO.composition_dao import CompositionDao
from Business_objects.avis import Avis

class TestDaoIngredient(TestCase):

    def test_create(self):
        """ Test de la création d'un nouvel ingrédient dans la base de données avec vérification
            qu'un id soit généré pour celui-ci
        """
        nom_ingredient = "ramune"
        id_ingredient = IngredientDao.create(nom_ingredient=nom_ingredient)
        self.assertIsNotNone(id_ingredient)
        IngredientDao.delete(id_ingredient=id_ingredient)

    def test_delete(self):
        """ Test de la suppression d'un nouvel ingrédient dans la base de données avec vérification
            que celui-ci soit bien supprimée
        """
        nom_ingredient = "ramune"
        id_ingredient = IngredientDao.create(nom_ingredient=nom_ingredient)
        deleted = IngredientDao.delete(id_ingredient=id_ingredient)
        self.assertTrue(deleted)

    def test_completion_ingredients(self):
        """ Test de la récupération des noms des ingrédients d'un cocktail
        """
        cocktail = CocktailDao.find_objet(id_cocktail=1)
        cocktail = CompositionDao.completion_ingredients(cocktail=cocktail)
        cocktail = IngredientDao.completion_ingredients(cocktail=cocktail)
        self.assertEquals(first=cocktail.ingredients[0].nom, second="Vodka")
        self.assertEquals(first=cocktail.ingredients[1].nom, second="Grenadine")
        self.assertEquals(first=cocktail.ingredients[2].nom, second="Jus multifruits")