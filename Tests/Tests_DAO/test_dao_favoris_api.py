from unittest import TestCase

from DAO.favoris_api_dao import FavorisApiDao

class TestDaoFavorisApi(TestCase):

    def test_create(self):
        """ Test de l'ajout d'un avis dans la base de données avec vérification
            qu'il est bien créé
        """
        created = FavorisApiDao.create(id_user=1, id_cocktail=11007)
        self.assertTrue(created)
        FavorisApiDao.delete(id_user=1, id_cocktail=11007)

    def test_delete(self):
        """ Test de la suppression d'un avis dans la base de données avec vérification
            qu'il est bien suprimé
        """
        FavorisApiDao.create(id_user=1, id_cocktail=11007)
        deleted = FavorisApiDao.delete(id_user=1, id_cocktail=11007)
        self.assertTrue(deleted)

    def test_find_by_id_user(self):
        """ Test de la récupération des avis écrits par utilisateur
        """
        FavorisApiDao.create(id_user=1, id_cocktail=11007)
        favoris = FavorisApiDao.find_by_id_user(id_user=1)
        self.assertIsNotNone(favoris)
        FavorisApiDao.delete(id_user=1, id_cocktail=11007)