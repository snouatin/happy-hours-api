from unittest import TestCase

from DAO.favoris_dao import FavorisDao

class TestDaoFavoris(TestCase):

    def test_create(self):
        """ Test de l'ajout d'un avis dans la base de données avec vérification
            qu'il est bien créé
        """
        created = FavorisDao.create(id_user=1, id_cocktail=1)
        self.assertTrue(created)
        FavorisDao.delete(id_user=1, id_cocktail=1)

    def test_delete(self):
        """ Test de la suppression d'un avis dans la base de données avec vérification
            qu'il est bien suprimé
        """
        FavorisDao.create(id_user=1, id_cocktail=1)
        deleted = FavorisDao.delete(id_user=1, id_cocktail=1)
        self.assertTrue(deleted)

    def test_find_by_id_user(self):
        """ Test de la récupération des avis écrits par utilisateur
        """
        FavorisDao.create(id_user=1, id_cocktail=1)
        favoris = FavorisDao.find_by_id_user(id_user=1)
        self.assertIsNotNone(favoris)
        FavorisDao.delete(id_user=1, id_cocktail=1)