from unittest import TestCase

from DAO.historique_dao import HistoriqueDao

class TestDaoHistorique(TestCase):

    def test_create(self):
        """ Test de l'ajout d'un avis dans la base de données avec vérification
            qu'il est bien créé
        """
        id_historique = HistoriqueDao.create(id_user=1, terme_recherche="grenadine")
        self.assertIsNotNone(id_historique)
        HistoriqueDao.delete(id_historique=id_historique)

    def test_delete(self):
        """ Test de la suppression d'un avis dans la base de données avec vérification
            qu'il est bien suprimé
        """
        id_historique = HistoriqueDao.create(id_user=1, terme_recherche="grenadine")
        deleted = HistoriqueDao.delete(id_historique=id_historique)
        self.assertTrue(deleted)

    def test_find_by_id_user(self):
        """ Test de la récupération des avis écrits par utilisateur
        """
        id_historique = HistoriqueDao.create(id_user=1, terme_recherche="grenadine")
        historique = HistoriqueDao.find_by_id_user(id_user=1)
        self.assertIsNotNone(historique)
        HistoriqueDao.delete(id_historique=id_historique)

    def test_get_all(self):
        """ Test la récupération de tout l'historique de l'application
        """
        id_historique = HistoriqueDao.create(id_user=1, terme_recherche="Grenadine")
        historique = HistoriqueDao.get_all()
        self.assertIsNotNone(historique)
        HistoriqueDao.delete(id_historique=id_historique)