from unittest import TestCase

from DAO.avis_dao import AvisDao
from Business_objects.avis import Avis

class TestDaoAvis(TestCase):

    def test_create(self):
        """ Test de l'ajout d'un avis dans la base de données avec vérification
            qu'il est bien créé
        """
        avis = Avis(id_user=1, id_cocktail=1, note=10, commentaire="C'est l'extase !")
        created = AvisDao.create(avis=avis)
        self.assertTrue(created)
        AvisDao.delete(avis=avis)

    def test_update(self):
        """ Test de la modification d'un avis dans la base de données avec vérification
            qu'il est bien modifié
        """
        avis = Avis(id_user=1, id_cocktail=1, note=10, commentaire="C'est l'extase !")
        AvisDao.create(avis=avis)
        avis.note = 9.5
        updated = AvisDao.update(avis=avis)
        self.assertTrue(updated)
        AvisDao.delete(avis=avis)

    def test_delete(self):
        """ Test de la suppression d'un avis dans la base de données avec vérification
            qu'il est bien suprimé
        """
        avis = Avis(id_user=1, id_cocktail=1, note=10, commentaire="C'est l'extase !")
        AvisDao.create(avis=avis)
        deleted = AvisDao.delete(avis=avis)
        self.assertTrue(deleted)

    def test_find_by_id_cocktail(self):
        """ Test de la récupération des avis d'un cocktail
        """
        avis = Avis(id_user=1, id_cocktail=1, note=10, commentaire="C'est l'extase !")
        AvisDao.create(avis=avis)
        liste_avis = AvisDao.find_by_id_cocktail(id_cocktail=1)
        self.assertIsNotNone(liste_avis)
        AvisDao.delete(avis=avis)