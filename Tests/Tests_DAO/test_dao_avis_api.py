from unittest import TestCase

from DAO.avis_api_dao import AvisApiDao
from Business_objects.avis import Avis

class TestDaoAvisApi(TestCase):

    def test_create(self):
        """ Test de l'ajout d'un avis dans la base de données avec vérification
            qu'il est bien créé
        """
        avis = Avis(id_user=1, id_cocktail=11007, note=6, commentaire="Bon mais pas mon délire :/")
        created = AvisApiDao.create(avis=avis)
        self.assertTrue(created)
        AvisApiDao.delete(avis=avis)

    def test_update(self):
        """ Test de la modification d'un avis dans la base de données avec vérification
            qu'il est bien modifié
        """
        avis = Avis(id_user=1, id_cocktail=11007, note=6, commentaire="Bon mais pas mon délire :/")
        AvisApiDao.create(avis=avis)
        avis.note = 6.5
        updated = AvisApiDao.update(avis=avis)
        self.assertTrue(updated)
        AvisApiDao.delete(avis=avis)

    def test_delete(self):
        """ Test de la suppression d'un avis dans la base de données avec vérification
            qu'il est bien suprimé
        """
        avis = Avis(id_user=1, id_cocktail=11007, note=6, commentaire="Bon mais pas mon délire :/")
        AvisApiDao.create(avis=avis)
        deleted = AvisApiDao.delete(avis=avis)
        self.assertTrue(deleted)

    def test_find_by_id_user(self):
        """ Test de la récupération des avis écrits par utilisateur
        """
        avis = Avis(id_user=1, id_cocktail=11007, note=6, commentaire="Bon mais pas mon délire :/")
        AvisApiDao.create(avis=avis)
        liste_avis = AvisApiDao.find_by_id_user(id_user=1)
        self.assertIsNotNone(liste_avis)
        AvisApiDao.delete(avis=avis)

    def test_find_by_id_cocktail(self):
        """ Test de la récupération des avis d'un cocktail
        """
        avis = Avis(id_user=1, id_cocktail=11007, note=6, commentaire="Bon mais pas mon délire :/")
        AvisApiDao.create(avis=avis)
        liste_avis = AvisApiDao.find_by_id_cocktail(id_cocktail=11007)
        self.assertIsNotNone(liste_avis)
        AvisApiDao.delete(avis=avis)

    def test_find_by_id_user_cocktail(self):
        """ Test de la récupération de l'avis d'un cocktail écrit par un utilisateur
        """
        avis = Avis(id_user=1, id_cocktail=11007, note=6, commentaire="Bon mais pas mon délire :/")
        AvisApiDao.create(avis=avis)
        recovered = AvisApiDao.find_by_id_user_cocktail(id_user=1, id_cocktail=11007)
        self.assertIsNotNone(recovered)
        AvisApiDao.delete(avis=avis)