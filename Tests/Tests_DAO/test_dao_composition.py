from unittest import TestCase

from DAO.ingredient_dao import IngredientDao
from Business_objects.cocktail import Cocktail
from Business_objects.ingredient import Ingredient
from DAO.composition_dao import CompositionDao
from DAO.cocktail_dao import CocktailDao

class TestDaoComposition(TestCase):

    def test_create(self):
        """ Test de la création d'une composition dans la base de données
        """
        cocktail = Cocktail(id_cocktail=None
                                , nom="Ramune"
                                , type = "Cocktail"
                                , alcool="Non Alcoholic"
                                , verre="Ordinary Glass"
                                , ingredients=[Ingredient(id_ingredient=IngredientDao.create(nom_ingredient="Ramune")
                                                            , nom="Ramune"
                                                            , mesure="6.76 oz")]
                                , recette=""
                                , URL=None
                                , sauvegarde="Bdd")
        cocktail = CocktailDao.create(cocktail=cocktail)
        created = CompositionDao.create(cocktail=cocktail)
        self.assertTrue(created)
        CocktailDao.delete(cocktail=cocktail)

    def test_update(self):
        """ Test de la modification d'une composition dans la base de données avec vérification
            que celle-ci soit bien modifiée
        """
        cocktail = Cocktail(id_cocktail=None
                                , nom="Ramune"
                                , type = "Cocktail"
                                , alcool="Non Alcoholic"
                                , verre="Ordinary Glass"
                                , ingredients=[Ingredient(id_ingredient=IngredientDao.create(nom_ingredient="Ramune")
                                                            , nom="Ramune"
                                                            , mesure="6.76 oz")]
                                , recette=""
                                , URL=None
                                , sauvegarde="Bdd")
        cocktail = CocktailDao.create(cocktail=cocktail)
        CompositionDao.create(cocktail=cocktail)
        cocktail.ingredients[0].mesure = "6.7 oz"
        updated = CompositionDao.update(cocktail=cocktail)
        self.assertTrue(updated)
        CocktailDao.delete(cocktail=cocktail)

    def test_completion_ingredients(self):
        """ Test de la complétion des ingrédients d'un cocktail
        """
        cocktail = CocktailDao.find_objet(id_cocktail=1)
        cocktail = CompositionDao.completion_ingredients(cocktail=cocktail)
        self.assertTrue(len(cocktail.ingredients)==3)
    
    def test_find_by_nom_ingredient(self):
        """ Test de la récupération des id des cocktails qui contiennent un ingrédient
        """
        nom_ingredient = "Grenadine"
        liste = CompositionDao.find_by_nom_ingredient(nom_ingredient=nom_ingredient)
        self.assertIsNotNone(liste)