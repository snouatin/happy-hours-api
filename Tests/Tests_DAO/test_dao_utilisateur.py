from unittest import TestCase

from DAO.avis_dao import AvisDao
from DAO.utilisateur_dao import UtilisateurDao
from Business_objects.utilisateur import Utilisateur

class TestDaoUtilisateur(TestCase):

    def test_create(self):
        """ Test de l'ajout d'un avis dans la base de données avec vérification
            qu'il est bien créé
        """
        utilisateur = Utilisateur(id_user=None
                                    , TYPE_BASE_DE_DONNEE="Administrateur"
                                    , identifiant="remip"
                                    , password="remip"
                                    , nom="pepin"
                                    , prenom="remi"
                                    , email=None)
        utilisateur = UtilisateurDao.create(utilisateur=utilisateur)
        self.assertIsNotNone(obj=utilisateur)
        UtilisateurDao.delete(utilisateur=utilisateur)

    def test_update(self):
        """ Test de la modification d'un avis dans la base de données avec vérification
            qu'il est bien modifié
        """
        utilisateur = Utilisateur(id_user=None
                                    , TYPE_BASE_DE_DONNEE="Administrateur"
                                    , identifiant="remip"
                                    , password="remip"
                                    , nom="pepin"
                                    , prenom="remi"
                                    , email=None)
        utilisateur = UtilisateurDao.create(utilisateur=utilisateur)
        utilisateur.email = "arthur@gg.com"
        updated = UtilisateurDao.update(utilisateur=utilisateur)
        self.assertTrue(updated)
        UtilisateurDao.delete(utilisateur=utilisateur)

    def test_delete(self):
        """ Test de la suppression d'un avis dans la base de données avec vérification
            qu'il est bien suprimé
        """
        utilisateur = Utilisateur(id_user=None
                                    , TYPE_BASE_DE_DONNEE="Administrateur"
                                    , identifiant="remip"
                                    , password="remip"
                                    , nom="pepin"
                                    , prenom="remi"
                                    , email=None)
        utilisateur = UtilisateurDao.create(utilisateur=utilisateur)
        deleted = UtilisateurDao.delete(utilisateur=utilisateur)
        self.assertTrue(deleted)

    def test_find_by_identifiant(self):
        """ Test de la récupération des avis écrits par utilisateur
        """
        identifiant = "artfav"
        utilisateur = UtilisateurDao.find_by_identifiant(identifiant=identifiant)
        self.assertIsNotNone(obj=utilisateur)

    def test_find_by_id_user(self):
        """ Test de la récupération des avis d'un cocktail
        """
        id_user = 1
        utilisateur = UtilisateurDao.find_by_id_user(id_user=id_user)
        self.assertIsNotNone(obj=utilisateur)

    def test_get_all(self):
        """ Test de la récupération de tous les utilisateurs de la base de données
        """
        liste_users = UtilisateurDao.get_all()
        arthur = liste_users[0]
        self.assertEquals(first=arthur.prenom, second="Arthur")