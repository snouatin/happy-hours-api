from unittest import TestCase

from DAO.cocktail_dao import CocktailDao
from Business_objects.cocktail import Cocktail

class TestDaoCocktail(TestCase):

    def test_create(self):
        """ Test de la création d'un cocktail dans la base de données avec vérification
            qu'un id soit généré pour celui-ci
        """
        cocktail = Cocktail(id_cocktail=None
                                , nom="Ramune"
                                , type="Cocktail"
                                , alcool="Non Alcoholic"
                                , verre="Cocktail Glass"
                                , ingredients=None
                                , recette=None
                                , URL=None
                                , sauvegarde="Bdd")
        cocktail = CocktailDao.create(cocktail=cocktail)
        self.assertIsNotNone(cocktail)
        CocktailDao.delete(cocktail)

    def test_accepter_suggestion(self):
        """
        """
        cocktail = Cocktail(id_cocktail=None
                                , nom="Ramune"
                                , type="Cocktail"
                                , alcool="Non Alcoholic"
                                , verre="Cocktail Glass"
                                , ingredients=None
                                , recette=None
                                , URL=None
                                , sauvegarde="Bdd")
        cocktail = CocktailDao.create(cocktail=cocktail)
        test = CocktailDao.accepter_suggestion(id_cocktail=cocktail.id)
        self.assertTrue(test)
        CocktailDao.delete(cocktail)

    def test_delete(self):
        """ Test de la suppression d'un cocktail dans la base de données avec vérification
            qu'il soit supprimer
        """
        cocktail = Cocktail(id_cocktail=None
                                , nom="Ramune"
                                , type="Cocktail"
                                , alcool="Non Alcoholic"
                                , verre="Cocktail Glass"
                                , ingredients=None
                                , recette=None
                                , URL=None
                                , sauvegarde="Bdd")
        cocktail = CocktailDao.create(cocktail=cocktail)
        deleted = CocktailDao.delete(cocktail)
        self.assertTrue(deleted)

    def test_update(self):
        """ Test de la modification d'un cocktail dans la base de données avec vérification
            qu'il soit modifier
        """
        cocktail = Cocktail(id_cocktail=None
                                , nom="Ramune"
                                , type="Cocktail"
                                , alcool="Non Alcoholic"
                                , verre="Cocktail Glass"
                                , ingredients=None
                                , recette=None
                                , URL=None
                                , sauvegarde="Bdd")
        cocktail = CocktailDao.create(cocktail=cocktail)
        cocktail.recette = "Test"
        updated = CocktailDao.update(cocktail=cocktail)
        self.assertTrue(updated)
        CocktailDao.delete(cocktail=cocktail)

    def test_find_objet_by_nom(self):
        """ Test de récupération d'un cocktail à partir de son nom
        """
        nom_cocktail = "Soupe"
        cocktail = CocktailDao.find_objet_by_nom(nom_cocktail=nom_cocktail)
        self.assertEquals(first=cocktail.nom, second=nom_cocktail)

    def test_find_objet(self):
        """ Test de la récupération d'un objet cocktail à partir de son id
        """
        cocktail = CocktailDao.find_objet(id_cocktail=1)
        self.assertEquals(first=cocktail.nom, second="Soupe")

    def test_find_suggestions(self):
        """ Test de la récupération des cours en attente de validation
        """
        cocktail = Cocktail(id_cocktail=None
                                , nom="Ramune"
                                , type="Cocktail"
                                , alcool="Non Alcoholic"
                                , verre="Cocktail Glass"
                                , ingredients=None
                                , recette=None
                                , URL=None
                                , sauvegarde="Bdd")
        cocktail = CocktailDao.create(cocktail=cocktail)
        suggestions = CocktailDao.find_suggestions()
        test = False
        for cocktail in suggestions :
            if cocktail.nom == "Ramune" :
                test = True
        self.assertTrue(test)
        CocktailDao.delete(cocktail)

    def test_find_by_alcool(self):
        """ Test de la récupération des cocktails en fonction de leur conteneur en alcool
        """
        alcool = "Alcoholic"
        liste = CocktailDao.find_by_alcool(alcool=alcool)
        test = True
        for cocktail in liste :
            if cocktail.alcool != alcool :
                test = False
        self.assertTrue(test)

    def test_find_by_verre(self):
        """ Test de la récupération des cocktails en fonction de leur contenenant
        """
        verre = "Cocktail Glass"
        liste = CocktailDao.find_by_verre(verre=verre)
        test = True
        for cocktail in liste :
            if cocktail.verre != verre :
                test = False
        self.assertTrue(test)

    def test_find_by_type(self):
        """ Test de la récupération des cocktails d'une certaine catégorie
        """
        type = "Cocktail"
        liste = CocktailDao.find_by_type(type=type)
        test = True
        for cocktail in liste :
            if cocktail.type != type :
                test = False
        self.assertTrue(test)