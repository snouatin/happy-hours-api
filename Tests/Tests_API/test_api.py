from unittest import TestCase
from API.api import Api

class TestApi(TestCase):

    def test_find_objet_by_nom(self):
        """ Test de la récupération d'un cocktail sur l'api
        """
        nom_cocktail = "Margarita"
        cocktail = Api.find_objet_by_nom(nom_cocktail=nom_cocktail)
        self.assertEquals(first=cocktail.nom, second=nom_cocktail)

    def test_find_objet(self):
        """ Test de la récupération d'un cocktail sur l'api
        """
        id_cocktail = 11007
        cocktail = Api.find_objet(id_cocktail=id_cocktail)
        self.assertEquals(first=cocktail.id, second=id_cocktail)

    def test_find_by_nom_ingredient(self):
        """ Test de la récupération des cocktail contenant un ingrédient particulier
        """
        nom_ingredient = "Vodka"
        cocktails = Api.find_by_nom_ingredient(nom_ingredient=nom_ingredient)
        self.assertIsNotNone(cocktails)

    def test_find_tous_verres(self):
        """
        """
        liste_verres = Api.find_tous_verres()
        self.assertIsNotNone(obj=liste_verres)