from unittest import TestCase
from Outils.fonctions_utiles import nom_format

class TestOutilsFonctionsUtilesDao(TestCase):

    def test_nom_format(self):
        """ Test de la mise en forme d'un string
        """
        texte = "leMon Player"
        outpout = nom_format(texte=texte)
        expected_outpout = "Lemon Player"
        self.assertEquals(first=outpout, second=expected_outpout)