from PyInquirer import prompt
from View.abstract_vue import AbstractView
from Services.utilisateurs_services import UtilisateurServices

class AvisView(AbstractView):

    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez vous faire ?',
                'choices': ['1. Modifier mon avis sur ce cocktail cocktail !'
                                , '2. Rien, retourner au cocktail !' ]
            }
        ]
        self.modif = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez modifier ?',
                'choices': ['1. La note !' 
                            , '2. Le commentaire !'
                            , '3. Les deux !'
                            , '4. Rien finalement ...' ]
            }
        ]
        self.note = [
            {
                'type': 'input',
                'name': 'note',
                'message': 'Quelle est votre nouvelle note sur 10 ?'
            }
        ]
        self.commentaire = [
            {
                'type': 'input',
                'name': 'commentaire',
                'message': 'Quel est votre nouveau commentaire ?'
            }
        ]

    @staticmethod
    def display_info():
        print('Voici votre avis sur le cocktail : \n')
        avis = AbstractView.session.avis
        message = '1. Note: {} , Commentaire: {} \n'.format(avis.note,avis.commentaire)
        print(message)

    def make_choice(self):
        reponse = prompt(self.questions)

        if reponse['choix'] == '1. Modifier mon avis sur ce cocktail cocktail !' :
            reponse = prompt(self.modif)

            if reponse['choix'] == '1. La note !' :
                reponse = prompt(self.note)
                AbstractView.session.avis.note = reponse['note']
                UtilisateurServices.modifier_avis(cocktail=AbstractView.session.cocktail,avis=AbstractView.session.avis)
                print('Vous avez modifié votre note sur ce cocktail \n')
                return AvisView()

            elif reponse['choix'] == '2. Le commentaire !' :
                reponse = prompt(self.commentaire)
                AbstractView.session.avis.commentaire = reponse['commentaire']
                UtilisateurServices.modifier_avis(cocktail=AbstractView.session.cocktail,avis=AbstractView.session.avis)
                print('Vous avez modifié votre commentaire sur ce cocktail \n')
                return AvisView()

            elif reponse['choix'] == '3. Les deux !' :
                reponse1 = prompt(self.note)
                reponse2 = prompt(self.commentaire)
                AbstractView.session.avis.note = reponse1['note']
                AbstractView.session.avis.commentaire = reponse2['commentaire']
                UtilisateurServices.modifier_avis(cocktail=AbstractView.session.cocktail,avis=AbstractView.session.avis)
                print('Vous avez modifié votre note et votre commentaire sur ce cocktail \n')
                return AvisView()

            elif reponse['choix'] == '4. Rien finalement ...' :
                return AvisView()

        elif reponse['choix'] == '2. Rien, retourner au cocktail !' :
            from View.cocktail_view import CocktailView
            return CocktailView()