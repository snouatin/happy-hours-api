from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from Services.utilisateurs_services import UtilisateurServices
from Services.suggestions_services import SuggestionsServices

class ChoisirSuggestionView(AbstractView):

    def __init__(self):
        """ 
        """
        self.questions = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Laquelle voulez vous choisir ?',
                'choices': AbstractView.session.mise_en_forme_suggestions
            }
        ]
        self.actions = [
            {
                'type': 'list',
                'name': 'action',
                'message': 'Que voulez faire de cette proposition ?',
                'choices': ['1. Accepter la proposition :D'
                                , '2. Supprimer la proposition :\'('
                                , '3. Ne rien faire finalement :/' ]
            }
        ]
        self.quitter = [
            {
                'type': 'list',
                'name': 'quitter',
                'message': 'Que voulez faire maintenant ?',
                'choices': ['1.Retourner aux propositions'
                                , '2. Retourner à l\'accueil'
                                , '3. Quitter l\'application' ]
            }
        ]

    @staticmethod
    def display_info():
        print('')

    def make_choice(self):
        suggestion = prompt(self.questions)
        action = prompt(self.actions)

        numero_suggestion = int(suggestion['choix'][0])

        if action['action'] == '1. Accepter la proposition :D' :
            all_suggestions = AbstractView.session.suggestions
            add_cocktail = all_suggestions[numero_suggestion-1]
            UtilisateurServices.accepter_suggestion(id_cocktail=add_cocktail.id)

        elif action['action'] == '2. Supprimer la proposition :\'(' :
            all_suggestions = AbstractView.session.suggestions
            delete_cocktail = all_suggestions[numero_suggestion-1]
            UtilisateurServices.rejeter_suggestion(cocktail=delete_cocktail)

        quitter = prompt(self.quitter)

        if quitter['quitter'] == '1.Retourner aux propositions' :
            AbstractView.session.suggestions = SuggestionsServices.get_all()
            if AbstractView.session.suggestions :
                AbstractView.session.mise_en_forme_suggestions = SuggestionsServices.mise_en_forme(suggestions=AbstractView.session.suggestions)
                from View.liste_suggestions_view import ListeSuggestionsView
                return ListeSuggestionsView()
            else :
                print('Il n\'y plus de suggestions :\'( \n')
                from View.admin_view import AdminView
                return AdminView()

        elif quitter['quitter'] == '2. Retourner à l\'accueil' :
            from View.admin_view import AdminView
            return AdminView()

        elif quitter['quitter'] == '3. Quitter l\'application' :
            from View.farewell_view import FarewellView
            return FarewellView()