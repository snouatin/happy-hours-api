from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView

class StartView(AbstractView):

    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Bienvenue à l\'Happy Hour !',
                'choices': ['. Entrer !']
            }
        ]

    def display_info(self):
        with open('assets/banner.txt', 'r', encoding="utf-8") as asset:
            print(asset.read())

    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse['choix'] == '. Entrer !':
            from View.welcome_view import WelcomeView
            return WelcomeView()