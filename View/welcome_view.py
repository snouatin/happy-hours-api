from PyInquirer import prompt
from View.abstract_vue import AbstractView
from Services.visiteur_services import VisiteurServices
from Services.recherche_services import RechercheServices

class WelcomeView(AbstractView):

    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'choix_menu',
                'message': 'Que voulez-vous faire ?',
                'choices': ['1. Boire, je suis déshydraté :)'
                            , '2. Me connecter !'
                            , '3. M\'inscrire pour commencer !'
                            , '4. Quitter, boire ne m\'intéresse plus :/' ]
            }
        ]

    @staticmethod
    def display_info():
        print('')

    def make_choice(self):

        reponse = prompt(self.questions)
        if reponse['choix_menu'] == '1. Boire, je suis déshydraté :)':
            AbstractView.session.user = None
            AbstractView.session.recherche = RechercheServices()
            from View.recherche_view import RechercheView
            return RechercheView()

        elif reponse['choix_menu'] == '2. Me connecter !':
            from View.sign_in_view import SignInView
            return SignInView()

        elif reponse['choix_menu'] == '3. M\'inscrire pour commencer !':
            from View.register_view import RegisterView
            return RegisterView()

        elif reponse['choix_menu'] == '4. Quitter, boire ne m\'intéresse plus :/' :
            from View.farewell_view import FarewellView
            return FarewellView()