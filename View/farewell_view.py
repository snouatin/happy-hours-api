from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView

class FarewellView(AbstractView):

    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Au revoir et à bientôt !',
                'choices': ['1. Quitter']
            }
        ]

    def display_info(self):
        with open('assets/banner.txt', 'r', encoding="utf-8") as asset:
            print(asset.read())

    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse['choix'] == '1. Quitter':
            print('')