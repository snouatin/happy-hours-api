from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from Services.recherche_services import RechercheServices
from Outils.fonctions_utiles import nom_format
from Services.mise_en_forme_services import MiseEnFormeServices
from Services.suggestions_services import SuggestionsServices
from Services.historique_services import HistoriqueServices

class RechercheView(AbstractView):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'actions',
                'message': 'Que voulez vous faire ?',
                'choices': ['1. Je connais le nom d\'un cocktail ;)'
                                , '2. Il me faut un ingrédient spécifique !'
                                , '3. Sa conteneur en alcool doit me convenir :D'
                                , '4. Je veux que son contenant soit particulier :p'
                                , '5. Je veux une catégorie de cocktails particulière :p'
                                , '6. Retourner au menu principal :/' ]
            }
        ]
        self.cocktail = [
            {
                'type': 'input',
                'name': 'cocktail',
                'message': 'Quel est le nom de ce cocktail ?'
            }
        ]
        alcools = AbstractView.session.recherche.liste_alcools()
        mis_en_forme_alcool = MiseEnFormeServices.mise_en_forme(liste=alcools)
        self.alcool = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Choisissez une de ces conteneurs en alcool disponibles :',
                'choices': mis_en_forme_alcool
            }
        ]
        verres = AbstractView.session.recherche.liste_verres()
        mis_en_forme_verre = MiseEnFormeServices.mise_en_forme(liste=verres)
        self.verre = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Choisissez un de ces contenants disponibles :',
                'choices': mis_en_forme_verre
            }
        ]
        types = AbstractView.session.recherche.liste_types()
        mis_en_forme_type = MiseEnFormeServices.mise_en_forme(liste=types)
        self.type = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Choisissez une de ces catégories de cocktails :',
                'choices': mis_en_forme_type
            }
        ]

    @staticmethod
    def display_info():
        print('Recherche de cocktail : \n')

    def make_choice(self):
        reponse = prompt(self.questions)

        if reponse["actions"] == '1. Je connais le nom d\'un cocktail ;)':
            nom_cocktail = prompt(self.cocktail)
            nom_cocktail = nom_cocktail['cocktail']
            nom_cocktail = nom_format(texte=nom_cocktail)
            if AbstractView.session.user :
                HistoriqueServices.add_historique(id_user=AbstractView.session.user.id,terme=nom_cocktail)
            cocktail = AbstractView.session.recherche.find_by_nom_cocktail(nom_cocktail=nom_cocktail)
            if cocktail :
                AbstractView.session.cocktail = cocktail
                from View.cocktail_view import CocktailView
                return CocktailView()
            else : 
                print('Le cocktail que vous cherchez est incompatible avec votre recherche :/')
                return RechercheView()

        elif reponse["actions"] == '2. Il me faut un ingrédient spécifique !':
            from View.ingredient_view import IngredientView
            return IngredientView()

        elif reponse["actions"] == '3. Sa conteneur en alcool doit me convenir :D':
            recherche = AbstractView.session.recherche
            if recherche.recherche_bdd.final_alcool :
                print('Cette recherche ne vous mènerait à rien :/ \n')
            else :
                alcool = prompt(self.alcool)
                alcool = alcool['choix'][3:]
                if AbstractView.session.user :
                    HistoriqueServices.add_historique(id_user=AbstractView.session.user.id,terme=alcool)
                AbstractView.session.recherche.find_by_alcool(alcool=alcool)
                liste_cocktails = AbstractView.session.recherche.liste_cocktails()
                mis_en_forme = SuggestionsServices.mise_en_forme(suggestions=liste_cocktails)
                print('Voici les cocktails qui correspondent à votre recherche : \n')
                for cocktail in mis_en_forme :
                    print(cocktail)
            return RechercheView()

        elif reponse["actions"] == '4. Je veux que son contenant soit particulier :p':
            recherche = AbstractView.session.recherche
            if recherche.recherche_bdd.final_verre :
                print('Cette recherche ne vous mènerait à rien :/ \n')
            else :
                verre = prompt(self.verre)
                verre = verre['choix'][3:]
                if AbstractView.session.user :
                    HistoriqueServices.add_historique(id_user=AbstractView.session.user.id,terme=verre)
                AbstractView.session.recherche.find_by_verre(verre=verre)
                liste_cocktails = AbstractView.session.recherche.liste_cocktails()
                mis_en_forme = SuggestionsServices.mise_en_forme(suggestions=liste_cocktails)
                print('Voici les cocktails qui correspondent à votre recherche : \n')
                for cocktail in mis_en_forme :
                    print(cocktail)
            return RechercheView()

        elif reponse["actions"] == '5. Je veux une catégorie de cocktails particulière :p':
            recherche = AbstractView.session.recherche
            if recherche.recherche_bdd.final_type :
                print('Cette recherche ne vous mènerait à rien :/ \n')
            else :
                type = prompt(self.type)
                type = type['choix'][3:]
                if AbstractView.session.user :
                    HistoriqueServices.add_historique(id_user=AbstractView.session.user.id,terme=type)
                AbstractView.session.recherche.find_by_type(type=type)
                liste_cocktails = AbstractView.session.recherche.liste_cocktails()
                mis_en_forme = SuggestionsServices.mise_en_forme(suggestions=liste_cocktails)
                print('Voici les cocktails qui correspondent à votre recherche : \n')
                for cocktail in mis_en_forme :
                    print(cocktail)
            return RechercheView()

        elif reponse["actions"] == '6. Retourner au menu principal :/':

            if AbstractView.session.user :

                if AbstractView.session.user.type == "Administrateur" :
                    from View.admin_view import AdminView
                    return AdminView()

                elif AbstractView.session.user.type == "Regulier" :
                    from View.regulier_view import RegulierView
                    return RegulierView()

            else :
                from View.welcome_view import WelcomeView
                return WelcomeView()