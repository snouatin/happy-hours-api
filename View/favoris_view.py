from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from Services.utilisateurs_services import UtilisateurServices
from Services.favoris_services import FavorisServices

class FavorisView(AbstractView):

    def __init__(self):
        """
        """
        self.actions = [
            {
                'type': 'list',
                'name': 'action',
                'message': 'Que voulez faire dans vos favoris ?',
                'choices': ['1. Consulter un cocktail de mes favoris'
                                , '2. Supprimer un cocktail de mes favoris'
                                , '3. Ne rien faire finalement :/' ]
            }
        ]
        self.choix = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Quel cocktail voulez vous regarder ?',
                'choices': AbstractView.session.mise_en_forme_favoris
            }
        ]

    @staticmethod
    def display_info():
        print('Voici vos favoris {} : \n'.format(AbstractView.session.user.prenom))
        favoris = AbstractView.session.mise_en_forme_favoris
        for cocktail in favoris :
            print(cocktail)

    def make_choice(self):
        action = prompt(self.actions)

        if action['action'] == '1. Consulter un cocktail de mes favoris' :
            choix = prompt(self.choix)
            numero = int(choix['choix'][0])-1
            AbstractView.session.cocktail = AbstractView.session.favoris[numero]
            from View.cocktail_view import CocktailView
            return CocktailView()

        elif action["action"] == '2. Supprimer un cocktail de mes favoris' :
            choix = prompt(self.choix)
            numero = int(choix['choix'][0])-1
            AbstractView.session.cocktail = AbstractView.session.favoris[numero]
            deleted = UtilisateurServices.delete_favoris(utilisateur=AbstractView.session.user
                                                            , cocktail=AbstractView.session.cocktail)
            if deleted :
                print('Vous avez retiré ce cocktail de vos favoris ! \n')
                AbstractView.session.user.chargement_favoris()
                if AbstractView.session.user.favoris_bdd or AbstractView.session.user.favoris_api :
                    AbstractView.session.favoris = FavorisServices.chargement(AbstractView.session.user)
                    AbstractView.session.mise_en_forme_favoris = FavorisServices.mise_en_forme(favoris=AbstractView.session.favoris)
                    from View.favoris_view import FavorisView
                    return FavorisView()
                else :
                    print('Vos favoris sont vides :O \n')

                    if AbstractView.session.user.type == "Regulier" :
                        from View.regulier_view import RegulierView
                        return RegulierView()

                    elif AbstractView.session.user.type == "Administrateur" :
                        from View.admin_view import AdminView
                        return AdminView()
            else :
                print('Erreur lors de la suppression de ce cocktail de vos favoris ! \n')
                return FavorisView()

        elif action["action"] == '3. Ne rien faire finalement :/' :

            if AbstractView.session.user.type == "Regulier" :
                from View.regulier_view import RegulierView
                return RegulierView()

            elif AbstractView.session.user.type == "Administrateur" :
                from View.admin_view import AdminView
                return AdminView()
