from PyInquirer import Separator, prompt
import hashlib
from View.abstract_vue import AbstractView
from View.register_view import PasswordValidator
from Services.utilisateurs_services import UtilisateurServices

class ModificationPasswordView(AbstractView):
    def __init__(self):
        self.password = [
            {
                'type': 'password',
                'name': 'password',
                'message': 'Entrez votre nouveau mot de passe : ',
                'validate': PasswordValidator
            }
        ]
        self.verif_password = [
            {
                'type': 'password',
                'name': 'password',
                'message': 'Entrez de nouveau le mot de passe : '
            }
        ]
        self.verif_incorrecte = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Mauvaise validation du mot de passe :/',
                'choices': ['1. Réessayer'
                            , '2. Retourner à la page de mon compte' ]
            }
        ]
        self.erreur = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez vous faire ?',
                'choices': ['1. Réessayer'
                            , '2. Rien, retourner à la page de mon compte' ]
            }
        ]

    @staticmethod
    def display_info():
        print('Modification de votre mot de passe : \n')

    def make_choice(self):

        reponse = prompt(self.password)
        verif = prompt(self.verif_password)

        if verif['password'] == reponse['password'] :
            password_hashed = hashlib.sha256(reponse['password'].encode()).hexdigest()
            password = str(password_hashed)
            AbstractView.session.user.password == password
            updated = UtilisateurServices.update(utilisateur=AbstractView.session.user)

            if updated :
                AbstractView.session.compte_utilisateur.password = AbstractView.session.user.password
                print('Mise à jour de votre mot de passe réussi ! \n')
                from View.compte_utilisateur_view import CompteUtilisateurView
                return CompteUtilisateurView()

            else :
                print('Erreur lors de la mise à jour de votre de passe !')
                erreur = prompt(self.erreur)

                if erreur['choix'] == '1. Réessayer' :
                    return ModificationPasswordView()

                elif erreur['choix'] == '2. Rien, retourner à la page de mon compte' :
                    from View.compte_utilisateur_view import CompteUtilisateurView
                    return CompteUtilisateurView()

        elif verif['password'] != reponse['password'] :
            erreur = prompt(self.verif_incorrecte)

            if erreur['choix'] == '1. Réessayer' :
                return ModificationPasswordView()

            elif erreur['choix'] == '2. Retourner à la page de mon compte' :
                from View.compte_utilisateur_view import CompteUtilisateurView
                return CompteUtilisateurView()