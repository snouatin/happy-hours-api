from PyInquirer import Separator, prompt, Validator, ValidationError
import hashlib
from View.abstract_vue import AbstractView
from Services.visiteur_services import VisiteurServices

class SignInView(AbstractView):

    def __init__(self):
        self.questions = [
            {
                'type': 'input',
                'name': 'username',
                'message': 'Entrez votre nom d\'utilisateur : '

            },
            {
                'type': 'password',
                'name': 'password',
                'message': 'Entrez votre mot de passe : '
            }
        ]
        self.echec = [
            {
                'type': 'list',
                'name': 'choix_menu',
                'message': 'Combinaison incorrecte, voulez réessayer ?',
                'choices': ['1. Oui, j\'ai des choses à faire :)'
                            , '2. Non, j\'ai plus le temps, je m\'en vais :/']
            }
        ]

    @staticmethod
    def display_info():
        print('C\'est parti, vous serez bientôt connecté !')

    def make_choice(self):
        answers = prompt(self.questions)
        password_hashed = hashlib.sha256(answers['password'].encode()).hexdigest()
        password = str(password_hashed)
        utilisateur = VisiteurServices.connexion(identifiant=answers['username'],password=password)
        if utilisateur :
            AbstractView.session.user = utilisateur
            if utilisateur.type == "Regulier" :
                from View.regulier_view import RegulierView
                return RegulierView()
            elif utilisateur.type == "Administrateur" :
                from View.admin_view import AdminView
                return AdminView()
        else :
            reponse = prompt(self.echec)
            if reponse['choix_menu'] == '2. Non, j\'ai plus le temps, je m\'en vais :/':
                from View.farewell_view import FarewellView
                return FarewellView()
            elif reponse['choix_menu'] == '1. Oui, j\'ai des choses à faire :)':
                from View.sign_in_view import SignInView
                return SignInView()