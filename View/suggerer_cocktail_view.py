from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from Business_objects.cocktail import Cocktail
from Services.ingredient_services import IngredientServices
from Services.cocktail_services import CocktailServices
from Services.liste_choix_services import ListeChoixServices
from Outils.fonctions_utiles import nom_format

class SuggererCocktailView(AbstractView):

    def __init__(self):

        liste_types = ListeChoixServices.categorie()
        liste_verres = ListeChoixServices.verre()
        liste_alcools = ListeChoixServices.alcool()

        self.questions = [
            {
                'type': 'input',
                'name': 'name',
                'message': 'Quel est le nom de votre cocktail ?'
            },
            {
                'type': 'list',
                'name': 'choix_type',
                'message': 'A quelle catégorie appartient votre cocktail ?',
                'choices': liste_types
            },
            {
                'type': 'list',
                'name': 'choix_verre',
                'message': 'Quel verre convient le mieux à votre cocktail ?',
                'choices': liste_verres
            },
            {
                'type': 'list',
                'name': 'choix_alcool',
                'message': 'Comment est alcoolisé votre cocktail ?',
                'choices': liste_alcools
            },
            {
                'type': 'input',
                'name': 'nombre ingredients',
                'message': 'Combien y a-t-il d\'ingrédients dans votre cocktail ?'
            }
        ]

    def display_info(self):
        print('Félicitations, vous allez créer votre propre cocktail, vous devenez un grand ! \n')

    def make_choice(self):
        reponses = prompt(self.questions)

        # Nom du cocktail 
        nom_cocktail = reponses['name']
        nom_cocktail = nom_format(texte=nom_cocktail)
        # Choix du verre
        verre = reponses['choix_verre']
        # Choix de la conteneur en alcool
        alcool = reponses['choix_alcool']
        # Choix de la conteneur en alcool
        type = reponses['choix_type']
        # Liste des ingrédients
        liste_ingredients = []
        nombre = int(reponses['nombre ingredients'])
        for i in range(nombre):
            description_ingredient = [
                {
                    'type': 'input',
                    'name': 'nom',
                    'message': 'Quel est le {} ingredient ?'.format(str(i+1))
                },
                {
                    'type': 'input',
                    'name': 'unite',
                    'message': 'Quelle est l\'unité de mesure de cet ingrédient ?'
                },
                {
                    'type': 'input',
                    'name': 'quantite',
                    'message': 'Et quelle est la quantité de cet ingrédient ?'
                }
            ]
            reponses = prompt(description_ingredient)
            nom = reponses['nom']
            nom = nom_format(texte=nom)
            unite = reponses['unite']
            quantite = reponses['quantite']
            mesure = quantite + " " + unite
            ingredient = IngredientServices.create(nom=nom, mesure=mesure)
            liste_ingredients += [ingredient]

        # Description de la recette
        recette = [
            {
                'type': 'input',
                'name': 'recette',
                'message': 'Une petite recette à nous donner ?'
            }
        ]
        description = prompt(recette)['recette']

        # Création du cocktail
        cocktail = Cocktail(id_cocktail=None
                            , nom=nom_cocktail
                            , type=type
                            , alcool=alcool
                            , verre=verre
                            , ingredients=liste_ingredients
                            , recette=description
                            , URL=None
                            , sauvegarde='bdd')
        
        cocktail = CocktailServices.create(cocktail=cocktail)
        print("Féicitations, votre cocktail sera peut-être bientôt accessible à tous !")
        
        if AbstractView.session.user.type == "Regulier" :
            from View.regulier_view import RegulierView
            return RegulierView()
        
        elif AbstractView.session.user.type == "Administrateur" :
            from View.admin_view import AdminView
            return AdminView()