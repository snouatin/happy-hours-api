from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView

class ComptesUtilisateursView(AbstractView):
    def __init__(self):
        self.actions = [
            {
                'type': 'list',
                'name': 'action',
                'message': 'Que souhaitez vous faire ?',
                'choices': ['1. Modifier un compte'
                            , '2. Rien, retourner à la page d\'accueil serait très bien']
            }
        ]
        self.questions = [
            {
                'type': 'list',
                'name': 'questions',
                'message': 'Avec quel compte souhaitez vous intéragir ?',
                'choices': AbstractView.session.mise_en_forme_comptes
            }
        ]

    @staticmethod
    def display_info():
        print('Voici les comptes de tous les autres utilisateurs ! \n')
        liste = AbstractView.session.mise_en_forme_comptes
        for utilisateur in liste :
            print(utilisateur)

    def make_choice(self):
        reponse = prompt(self.actions)

        if reponse["action"] == '1. Modifier un compte':
            reponse = prompt(self.questions)
            numero_compte = int(reponse["questions"][0])-1
            AbstractView.session.compte_utilisateur = AbstractView.session.comptes_utilisateurs[numero_compte]
            from View.compte_utilisateur_view import CompteUtilisateurView
            return CompteUtilisateurView()

        elif reponse["action"] == '2. Rien, retourner à la page d\'accueil serait très bien':
            from View.admin_view import AdminView
            return AdminView()