from Business_objects.utilisateur import Utilisateur

class Session:
    
    def __init__(self):
        """ Définition de variables que l'on stocke en session
            ref:type = valeur
        """
        self.user = None
        self.recherche = None
        self.cocktail = None
        self.avis = None
        self.historique = None
        self.mise_en_forme_historique = None
        self.compte_utilisateur = None
        self.comptes_utilisateurs = None
        self.mise_en_forme_comptes = None
        self.suggestions = None
        self.mise_en_forme_suggestions = None
        self.favoris = None
        self.mise_en_forme_favoris = None