from PyInquirer import prompt
import urllib.request
# from PIL import Image
from View.abstract_vue import AbstractView
from Services.mise_en_forme_services import MiseEnFormeServices
from Services.cocktail_services import CocktailServices
from Services.utilisateurs_services import UtilisateurServices

class CocktailView(AbstractView):

    def __init__(self):
        self.administrateur = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez-vous faire ?',
                'choices': ['1. Consulter les avis du cocktails ;)'
                            , '2. Poster un avis sur ce cocktail !'
                            , '3. Ajouter le cocktail à mes favoris !'
                            , '4. Supprimer ce cocktail de la base de données !'
                            , '5. Retour à l\'accueil :)' ]
            }
        ]
        self.regulier = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez-vous faire ?',
                'choices': ['1. Consulter les avis du cocktails ;)'
                            , '2. Poster un avis sur ce cocktail !'
                            , '3. Ajouter le cocktail à mes favoris !'
                            , '4. Retour à l\'accueil :)' ]
            }
        ]
        self.visiteur = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez-vous faire ?',
                'choices': ['1. Consulter les avis du cocktails ;)'
                            , '2. Retour à l\'accueil :)' ]
            }
        ]
        self.avis = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez-vous consulter ?',
                'choices': ['1. Mon avis sur le cocktail'
                            , '2. Tous les avis' ]
            }
        ]
        self.note = [
            {
                'type': 'input',
                'name': 'note',
                'message': 'Quelle note sur 10 donnez vous au cocktail ?'
            }
        ]
        self.commentaire = [
            {
                'type': 'input',
                'name': 'commentaire',
                'message': 'Quel est votre ressenti sur ce cocktail ?'
            }
        ]

    @staticmethod
    def display_info():
        print('Voici le cocktail que vous avez tant cherché ! \n')
        cocktail = AbstractView.session.cocktail
        mis_en_forme = MiseEnFormeServices.mise_en_forme_cocktail(cocktail=cocktail)
        print(mis_en_forme)
        """
        if AbstractView.session.cocktail.sauvegarde == "Api" :
            url = AbstractView.session.cocktail.URL
            image = Image.open(urllib. request. urlopen(url))
            width,height = image.size
            print (width,height)
        """

    def make_choice(self):

        if AbstractView.session.user :

            if AbstractView.session.user.type == "Administrateur" :
                reponse = prompt(self.administrateur)

                if reponse['choix'] == '1. Consulter les avis du cocktails ;)' :
                    AbstractView.session.cocktail = CocktailServices.chargement_avis(cocktail=AbstractView.session.cocktail)
                    if AbstractView.session.cocktail.avis :
                        reponse = prompt(self.avis)
                    else : 
                        print('Il n\'y a aucun avis sur ce cocktail :/ \n')
                        return CocktailView()

                    if reponse['choix'] == '2. Tous les avis' :
                        from View.tous_avis_view import TousAvisView
                        return TousAvisView()

                    elif reponse['choix'] == '1. Mon avis sur le cocktail' :
                        AbstractView.session.cocktail = CocktailServices.chargement_avis(cocktail=AbstractView.session.cocktail)
                        for avis in AbstractView.session.cocktail.avis :
                            if avis.id_user == AbstractView.session.user.id :
                                AbstractView.session.avis = avis
                        if AbstractView.session.avis :
                            from View.avis_view import AvisView
                            return AvisView()
                        else :
                            print('Vous n\'avez pas encore posté d\'avis sur ce cocktail : \n')
                            return CocktailView()

                elif reponse['choix'] == '2. Poster un avis sur ce cocktail !' :
                    note = prompt(self.note)
                    commentaire = prompt(self.commentaire)
                    UtilisateurServices.ajouter_avis(id_user=AbstractView.session.user.id
                                                        , cocktail=AbstractView.session.cocktail
                                                        , note=note['note']
                                                        , commentaire=commentaire['commentaire'])
                    print('Félicitations, vous avez ajouté un avis sur ce cocktail ! \n')
                    return CocktailView()
                
                elif reponse['choix'] == '3. Ajouter le cocktail à mes favoris !' :
                    AbstractView.session.user.chargement_favoris()
                    if  AbstractView.session.user.favoris_bdd or AbstractView.session.user.favoris_api :
                        utilisateur = AbstractView.session.user
                        cocktail = AbstractView.session.cocktail
                        if utilisateur.favoris_bdd and cocktail.id in utilisateur.favoris_bdd :
                            print('Ce cocktail est déjà dans vos favoris ;) \n')
                            return CocktailView()
                        elif utilisateur.favoris_api and cocktail.id in utilisateur.favoris_api :
                            print('Ce cocktail est déjà dans vos favoris ;) \n')
                            return CocktailView()
                        else :
                            UtilisateurServices.ajouter_favoris(utilisateur=utilisateur, cocktail=cocktail)
                            print('Vous avez ajouté ce cocktail à vos favoris :D \n')
                            return CocktailView()
                    else :
                        utilisateur = AbstractView.session.user
                        cocktail = AbstractView.session.cocktail
                        UtilisateurServices.ajouter_favoris(utilisateur=utilisateur, cocktail=cocktail)
                        print('Vous avez ajouté ce cocktail à vos favoris :D \n')
                        return CocktailView()
                
                elif reponse['choix'] == '4. Supprimer ce cocktail de la base de données !' :
                    if AbstractView.session.cocktail.sauvegarde == "Bdd" :
                        deleted = UtilisateurServices.supprimer_coktail(cocktail=AbstractView.session.cocktail)
                        if deleted :
                            print('Suppression réussie, vous deviez détester ce cocktail xD \n')
                            from View.admin_view import AdminView
                            return AdminView()
                        else :
                            print('Echec lors de la suppresion :/ \n')
                            return CocktailView()
                    elif AbstractView.session.cocktail.sauvegarde == "Api" :
                        print('Vous ne pouvez pas supprimer ce cocktail (api) :/ \n')
                        return CocktailView()

                elif reponse['choix'] == '5. Retour à l\'accueil :)' :
                    from View.admin_view import AdminView
                    return AdminView()

            elif AbstractView.session.user.type == "Regulier" :
                reponse = prompt(self.regulier)

                if reponse['choix'] == '1. Consulter les avis du cocktails ;)' :
                    reponse = prompt(self.avis)

                    if reponse['choix'] == '2. Les avis des autres utilisateurs' :
                        AbstractView.session.cocktail = CocktailServices.chargement_avis(cocktail=AbstractView.session.cocktail)
                        if AbstractView.session.cocktail.avis :
                            from View.tous_avis_view import TousAvisView
                            return TousAvisView()
                        else :
                            print('Il n\'y a aucun avis sur ce cocktail :/ \n')
                            return CocktailView()

                    elif reponse['choix'] == '1. Mon avis sur le cocktail' :
                        AbstractView.session.cocktail = CocktailServices.chargement_avis(cocktail=AbstractView.session.cocktail)
                        for avis in AbstractView.session.cocktail :
                            if avis.id_user == AbstractView.session.user.id :
                                AbstractView.session.avis = avis
                        if AbstractView.session.avis :
                            from View.avis_view import AvisView
                            return AvisView()
                        else :
                            print('Vous n\'avez pas encore posté d\'avis sur ce cocktail : \n')
                            return CocktailView()

                elif reponse['choix'] == '2. Poster un avis sur ce cocktail !' :
                    note = prompt(self.note)
                    commentaire = prompt(self.commentaire)
                    UtilisateurServices.ajouter_avis(id_user=AbstractView.session.user.id
                                                        , cocktail=AbstractView.session.cocktail
                                                        , note=note['note']
                                                        , commentaire=commentaire['commentaire'])
                    print('Félicitations, vous avez ajouté un avis sur ce cocktail ! \n')
                    return CocktailView()

                elif reponse['choix'] == '3. Ajouter le cocktail à mes favoris !' :
                    AbstractView.session.user.chargement_favoris()
                    utilisateur = AbstractView.session.user
                    cocktail = AbstractView.session.cocktail
                    if utilisateur.favoris_bdd and cocktail.id in utilisateur.favoris_bdd :
                        print('Ce cocktail est déjà dans vos favoris ;) \n')
                        return CocktailView()
                    elif utilisateur.favoris_api and cocktail.id in utilisateur.favoris_api :
                        print('Ce cocktail est déjà dans vos favoris ;) \n')
                        return CocktailView()
                    else :
                        UtilisateurServices.ajouter_favoris(utilisateur=utilisateur, cocktail=cocktail)
                        print('Vous avez ajouté ce cocktail à vos favoris :D \n')
                        return CocktailView()

                elif reponse['choix'] == '4. Retour à l\'accueil :)' :
                    from View.regulier_view import RegulierView
                    return RegulierView()

        else :
            reponse = prompt(self.visiteur)

            if reponse['choix'] == '1. Consulter les avis du cocktails ;)' :
                AbstractView.session.cocktail = CocktailServices.chargement_avis(cocktail=AbstractView.session.cocktail)
                if AbstractView.session.cocktail.avis :
                    from View.tous_avis_view import TousAvisView
                    return TousAvisView()
                else :
                    print('Il n\'y a aucun avis sur ce cocktail :/ \n')
                    return CocktailView()

            elif reponse['choix'] == '2. Retour à l\'accueil :)' :
                from View.welcome_view import WelcomeView
                return WelcomeView()