from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from Services.utilisateurs_services import UtilisateurServices
from Services.historique_services import HistoriqueServices

class HistoriqueView(AbstractView):

    def __init__(self):
        self.actions = [
            {
                'type': 'list',
                'name': 'actions',
                'message': 'Que voulez vous faire ?',
                'choices': ['1. Supprimer une recherche de mon historique' 
                                , '2. Retourner au menu principal' ]
            }
        ]
        self.choix = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Quelle recherche voulez vous cacher ? ;)',
                'choices': AbstractView.session.mise_en_forme_historique
            }
        ]

    @staticmethod
    def display_info():
        print('Voici l\'historique de vos recherches : \n')
        for recherche in AbstractView.session.mise_en_forme_historique:
            recherche.replace('Utilisateur id: {} , ', '')
            print(recherche)

    def make_choice(self):
        reponse = prompt(self.actions)

        if reponse["actions"] == '1. Supprimer une recherche de mon historique':
            choix = prompt(self.choix)

            numero = int(choix["choix"][0])-1
            id_recherche = AbstractView.session.user.historique[numero]["id_historique"]
            UtilisateurServices.delete_historique(id_historique=id_recherche)
            AbstractView.session.user.chargement_historique()
            historique = AbstractView.session.user.historique
            AbstractView.session.mise_en_forme_historique = HistoriqueServices.mise_en_forme(historique=historique)
            return HistoriqueView()

        elif reponse["actions"] == '2. Retourner au menu principal':
            if AbstractView.session.user.type == "Regulier" :
                from View.regulier_view import RegulierView
                return RegulierView()
            elif AbstractView.session.user.type == "Administrateur" :
                from View.admin_view import AdminView
                return AdminView()