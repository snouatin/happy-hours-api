from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from Outils.fonctions_utiles import nom_format
from Services.suggestions_services import SuggestionsServices
from Services.historique_services import HistoriqueServices

class IngredientView(AbstractView):

    def __init__(self):
        self.ingredient = [
            {
                'type': 'input',
                'name': 'ingredient',
                'message': 'Quel ingrédient voulez vous dans votre cocktail ?'
            }
        ]
        self.error = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez vous faire ?',
                'choices': ['1. Réessayer'
                                , '2. Retourner à la recherche' ]
            }
        ]

    @staticmethod
    def display_info():
        liste_ingredients = AbstractView.session.recherche.liste_ingredients()
        for i in range(len(liste_ingredients)):
            print(str(i+1) + '. Nom ingrédient: {}'.format(liste_ingredients[i]))

    def make_choice(self):
        liste_ingredients = AbstractView.session.recherche.liste_ingredients()

        reponse = prompt(self.ingredient)
        nom = reponse['ingredient']
        nom = nom_format(texte=nom)
        HistoriqueServices.add_historique(id_user=AbstractView.session.user.id,terme=nom)

        if nom in liste_ingredients or len(liste_ingredients) == 0 :
            AbstractView.session.recherche.find_by_nom_ingredient(nom_ingredient=nom)
            liste_cocktails = AbstractView.session.recherche.liste_cocktails()
            mis_en_forme = SuggestionsServices.mise_en_forme(suggestions=liste_cocktails)
            print('Voici les cocktails qui correspondent à votre recherche : \n')
            for cocktail in mis_en_forme :
                print(cocktail)
            from View.recherche_view import RechercheView
            return RechercheView()

        else :
            print('Cet ingrédient n\'est pas compatible avec votre recherche ! \n')
            erreur = prompt(self.error)

            if erreur['choix'] == '1. Réessayer' :
                return IngredientView()

            elif erreur['choix'] == '2. Retourner à la recherche' :
                from View.recherche_view import RechercheView
                return RechercheView()