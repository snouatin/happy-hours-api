from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from Services.utilisateurs_services import UtilisateurServices
from Services.recherche_services import RechercheServices
from Services.historique_services import HistoriqueServices
from Services.favoris_services import FavorisServices

class RegulierView(AbstractView):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'actions',
                'message': 'Comment étancher votre soiffe ?',
                'choices': ['1. Trouver ma boisson :D'
                                , '2. Proposer ma nouvelle invention ;)'
                                , '3. Consulter mes coups de coeurs <3'
                                , '4. Vérifier mon taux d\'alcoolémie :\'('
                                , '5. Quitter l\'application :\'(' ]
            }
        ]

    def display_info(self):
        print('Oh, vous revoià {} ! \n'.format(AbstractView.session.user.prenom))

    def make_choice(self):
        reponse = prompt(self.questions)

        if reponse["actions"] == '1. Trouver ma boisson :D':
            AbstractView.session.recherche = RechercheServices()
            from View.recherche_view import RechercheView
            return RechercheView()

        elif reponse["actions"] == '2. Proposer ma nouvelle invention ;)':
            from View.suggerer_cocktail_view import SuggererCocktailView
            return SuggererCocktailView()

        elif reponse["actions"] == '3. Consulter mes coups de coeurs <3':
            AbstractView.session.user.chargement_favoris()
            if AbstractView.session.user.favoris_bdd or AbstractView.session.user.favoris_api :
                AbstractView.session.favoris = FavorisServices.chargement(AbstractView.session.user)
                AbstractView.session.mise_en_forme_favoris = FavorisServices.mise_en_forme(favoris=AbstractView.session.favoris)
                from View.favoris_view import FavorisView
                return FavorisView()
            else :
                print('Vos favoris sont vides :O \n')
                return RegulierView()

        elif reponse["actions"] == '4. Vérifier mon taux d\'alcoolémie :\'(':
            AbstractView.session.user.chargement_historique()
            if AbstractView.session.user.historique :
                historique = AbstractView.session.user.historique
                AbstractView.session.mise_en_forme_historique = HistoriqueServices.mise_en_forme(historique=historique)
                from View.historique_view import HistoriqueView
                return HistoriqueView()
            else :
                print('Votre historique est vide :O \n')
                return RegulierView()

        elif reponse["actions"] == '5. Quitter l\'application :\'(':
            from View.farewell_view import FarewellView
            return FarewellView()