from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from DAO.historique_dao import HistoriqueDao
from Services.historique_services import HistoriqueServices

class AllHistoriqueView(AbstractView):

    def __init__(self):
        self.actions = [
            {
                'type': 'list',
                'name': 'actions',
                'message': 'Continuer :',
                'choices': ['1. Retourner au menu principal' ]
            }
        ]

    @staticmethod
    def display_info():
        print('Voici l\'historique de toutes les recherches : \n')
        historique = AbstractView.session.mise_en_forme_historique
        for recherche in historique :
            print(recherche)

    def make_choice(self):
        reponse = prompt(self.actions)

        if reponse["actions"] == '1. Retourner au menu principal':
            from View.admin_view import AdminView
            return AdminView()