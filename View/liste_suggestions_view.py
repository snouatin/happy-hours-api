from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from Services.utilisateurs_services import UtilisateurServices

class ListeSuggestionsView(AbstractView):

    def __init__(self):
        """ Récupère les cocktails en attente de validation et les propose à un administrateur
        """
        self.questions = [
            {
                'type': 'list',
                'name': 'actions',
                'message': 'Que voulez vous faire ?',
                'choices': ['1. Choisir une de ces inventions !'
                                , '2. Retourner à l\'accueil :/' ]
            }
        ]

    def display_info(self):
        print('Voici la liste des suggestions : \n')
        for cocktail in AbstractView.session.mise_en_forme_suggestions :
            print(cocktail)

    def make_choice(self):
        
        if AbstractView.session.suggestions == []:
            from View.admin_view import AdminView
            return AdminView()

        else :
            reponse = prompt(self.questions)

            if reponse['actions'] == '1. Choisir une de ces inventions !':
                from View.choisir_suggestion_view import ChoisirSuggestionView
                return ChoisirSuggestionView()

            elif reponse['actions'] == '2. Retourner à l\'accueil :/':
                AbstractView.session.previous_view = ListeSuggestionsView()
                from View.admin_view import AdminView
                return AdminView()