from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from Services.utilisateurs_services import UtilisateurServices

class ModificationEmailView(AbstractView):
    def __init__(self):
        self.email = [
            {
                'type': 'input',
                'name': 'email',
                'message': 'Entrez votre nouvel email : '
            }
        ]
        self.verif_email = [
            {
                'type': 'input',
                'name': 'email',
                'message': 'Entrez de nouveau l\'email : '
            }
        ]
        self.verif_incorrecte = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Mauvaise validation de l\'email :/',
                'choices': ['1. Réessayer'
                            , '2. Retourner à la page de mon compte' ]
            }
        ]
        self.erreur = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez vous faire ?',
                'choices': ['1. Réessayer'
                            , '2. Rien, retourner à la page de mon compte' ]
            }
        ]

    @staticmethod
    def display_info():
        print('Modification de votre email : \n')

    def make_choice(self):

        reponse = prompt(self.email)
        verif = prompt(self.verif_email)

        if verif['email'] == reponse['email'] :
            AbstractView.session.user.email == reponse['email']
            updated = UtilisateurServices.update(utilisateur=AbstractView.session.user)

            if updated :
                AbstractView.session.compte_utilisateur.email = AbstractView.session.user.email
                print('Mise à jour de votre email réussi ! \n')
                from View.compte_utilisateur_view import CompteUtilisateurView
                return CompteUtilisateurView()

            else :
                print('Erreur lors de la mise à jour de votre email !')
                erreur = prompt(self.erreur)

                if erreur['choix'] == '1. Réessayer' :
                    return ModificationEmailView()

                elif erreur['choix'] == '2. Rien, retourner à la page de mon compte' :
                    from View.compte_utilisateur_view import CompteUtilisateurView
                    return CompteUtilisateurView()

        elif verif['email'] != reponse['email'] :
            erreur = prompt(self.verif_incorrecte)

            if erreur['choix'] == '1. Réessayer' :
                return ModificationEmailView()

            elif erreur['choix'] == '2. Retourner à la page de mon compte' :
                from View.compte_utilisateur_view import CompteUtilisateurView
                return CompteUtilisateurView()