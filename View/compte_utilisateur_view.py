from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from Services.utilisateurs_services import UtilisateurServices
from Services.comptes_services import ComptesServices

class CompteUtilisateurView(AbstractView):
    def __init__(self):
        self.mon_compte = [
            {
                'type': 'list',
                'name': 'action',
                'message': 'Que voulez vous faire ?',
                'choices': ['1. Modifier le compte'
                            , '2. Rien, retourner à la page d\'accueil serait très bien' ]
            }
        ]
        self.modif_mon_compte = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez vous modifier ?',
                'choices': ['1. Mon mot de passe'
                            , '2. Mon email'
                            , '3. Rien finalement :/' ]
            }
        ]
        self.autre_compte = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez faire de ce compte ?',
                'choices': ['1. Passer le compte de Regulier à Administrateur'
                            , '2. Supprimer ce compte :/'
                            , '3. Rien, retourner à la page précédente serait très bien' ]
            }
        ]
        self.echec = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez vous faire ?',
                'choices': ['1. Réessayer !'
                            , '2. Passer, c\'est embêtant :/' ]
            }
        ]

    @staticmethod
    def display_info():
        print('Voici les informations de ce compte : \n')
        compte = AbstractView.session.compte_utilisateur
        print('Rang: ' + compte.type)
        print('Nom: ' + compte.nom)
        print('Prenom: ' + compte.prenom)
        print('Email: ' + compte.email)

    def make_choice(self):

        if AbstractView.session.user.id == AbstractView.session.compte_utilisateur.id :
            reponse = prompt(self.mon_compte)

            if reponse['action'] == '1. Modifier le compte' :
                modif = prompt(self.modif_mon_compte)

                if modif['choix'] == '1. Mon mot de passe' :
                    from View.modification_password_view import ModificationPasswordView
                    return ModificationPasswordView()

                elif modif['choix'] == '2. Mon email' :
                    from View.modification_email_view import ModificationEmailView
                    return ModificationPasswordView()

                elif modif['choix'] == '3. Rien finalement :/' :
                    
                    if AbstractView.session.user.type == "Administrateur" :
                        from View.admin_view import AdminView
                        return AdminView()

                    elif AbstractView.session.user.type == "Regulier" :
                        from View.regulier_view import RegulierView
                        return RegulierView()

            elif reponse['action'] == '2. Rien, retourner à la page d\'accueil serait très bien' :

                if AbstractView.session.user.type == "Administrateur" :
                    from View.admin_view import AdminView
                    return AdminView()

                elif AbstractView.session.user.type == "Regulier" :
                    from View.regulier_view import RegulierView
                    return RegulierView()

        else :
            reponse = prompt(self.autre_compte)

            if reponse['choix'] == '1. Passer le compte de Regulier à Administrateur' :
                AbstractView.session.compte_utilisateur.type = "Administrateur"
                updated = UtilisateurServices.update(utilisateur=AbstractView.session.compte_utilisateur)

                if updated :
                    print('Mise à jour réussie ! \n')
                    print('Vous êtes redirigé vers la page précédente \n')
                    AbstractView.session.comptes_utilisateurs = ComptesServices.get_all()
                    indice = None
                    for i in range(len(AbstractView.session.comptes_utilisateurs)) :
                        if AbstractView.session.comptes_utilisateurs[i].id == AbstractView.session.user.id :
                            indice = i
                    del AbstractView.session.comptes_utilisateurs[indice]
                    liste_comptes = AbstractView.session.comptes_utilisateurs
                    AbstractView.session.mise_en_forme_comptes = ComptesServices.mise_en_forme(comptes_utilisateurs=liste_comptes)
                    from View.comptes_utilisateurs_view import ComptesUtilisateursView
                    return ComptesUtilisateursView()

                else :
                    print('Echec lors de la mise à jour ! \n')
                    echec = prompt(self.echec)
                    
                    if echec['choix'] == '1. Réessayer !' :
                        return CompteUtilisateurView

                    elif echec['choix'] == '2. Passer, c\'est embêtant :/' :
                        AbstractView.session.compte_utilisateur.type = "Regulier"
                        from View.comptes_utilisateurs_view import ComptesUtilisateursView
                        return ComptesUtilisateursView()

            elif reponse['choix'] == '2. Supprimer ce compte :/' :
                compte = AbstractView.session.compte_utilisateur
                deleted = UtilisateurServices.supprimer_compte(utilisateur=compte)

                if deleted :
                    print('Suppression réussi :O \n')
                    print('Vous êtes redirigé \n')
                    AbstractView.session.comptes_utilisateurs = ComptesServices.get_all()
                    indice = None
                    for i in range(len(AbstractView.session.comptes_utilisateurs)) :
                        if AbstractView.session.comptes_utilisateurs[i].id == AbstractView.session.user.id :
                            indice = i
                    del AbstractView.session.comptes_utilisateurs[indice]
                    liste_comptes = AbstractView.session.comptes_utilisateurs
                    AbstractView.session.mise_en_forme_comptes = ComptesServices.mise_en_forme(comptes_utilisateurs=liste_comptes)
                    from View.comptes_utilisateurs_view import ComptesUtilisateursView
                    return ComptesUtilisateursView()

                else :
                    print('Erreur lors de la suppression :D \n')
                    echec = prompt(self.echec)

                    if echec['choix'] == '1. Réessayer !' :
                        return CompteUtilisateurView

                    elif echec['choix'] == '2. Passer, c\'est embêtant :/' :
                        from View.comptes_utilisateurs_view import ComptesUtilisateursView
                        return ComptesUtilisateursView()

            elif reponse['choix'] == '3. Rien, retourner à la page précédente serait très bien' :
                
                from View.comptes_utilisateurs_view import ComptesUtilisateursView
                return ComptesUtilisateursView()