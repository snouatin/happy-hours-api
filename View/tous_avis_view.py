from PyInquirer import prompt
from View.abstract_vue import AbstractView
from Services.mise_en_forme_services import MiseEnFormeServices
from Services.utilisateurs_services import UtilisateurServices
from Services.cocktail_services import CocktailServices

class TousAvisView(AbstractView):

    def __init__(self):
        """
        """
        self.autre = [
            {
                'type': 'list',
                'name': 'choix',
                'message': '',
                'choices': ['1. Retour au cocktail' ]
            }
        ]
        self.administrateur = [
            {
                'type': 'list',
                'name': 'choix',
                'message': '',
                'choices': ['1. Retour au cocktail' 
                            , '2. Supprimer un avis' ]
            }
        ]
        cocktail = AbstractView.session.cocktail
        mis_en_forme = MiseEnFormeServices.mise_en_forme_avis(cocktail=cocktail)
        self.avis = [
            {
                'type': 'list',
                'name': 'choix',
                'message': '',
                'choices': mis_en_forme
            }
        ]

    @staticmethod
    def display_info():
        print('Voici les avis du cocktail : \n')
        cocktail = AbstractView.session.cocktail
        mis_en_forme = MiseEnFormeServices.mise_en_forme_avis(cocktail=cocktail)
        for avis in mis_en_forme :
            print(avis)

    def make_choice(self):

        if AbstractView.session.user :

            if AbstractView.session.user.type == "Administrateur" :
                reponse = prompt(self.administrateur)

                if reponse['choix'] == '1. Retour au cocktail' :
                    from View.cocktail_view import CocktailView
                    return CocktailView()
                
                elif reponse['choix'] == '2. Supprimer un avis' :
                    reponse = prompt(self.avis)
                    numero = int(reponse['choix'][0])-1
                    avis = AbstractView.session.cocktail.avis[numero]
                    deleted = UtilisateurServices.delete_avis(avis=avis,cocktail=AbstractView.session.cocktail)
                    if deleted :
                        print('Suppression de l\'avis réussi ! \n ')
                        AbstractView.session.cocktail = CocktailServices.chargement_avis(cocktail=AbstractView.session.cocktail)
                        return TousAvisView
                    else : 
                        print('Erreur lors de la suppression :/ \n')
                        return TousAvisView

            else : 
                reponse = prompt(self.autre)
                if reponse['choix'] == '1. Retour au cocktail' :
                    from View.cocktail_view import CocktailView
                    return CocktailView()

        else : 
            reponse = prompt(self.autre)
            if reponse['choix'] == '1. Retour au cocktail' :
                from View.cocktail_view import CocktailView
                return CocktailView()