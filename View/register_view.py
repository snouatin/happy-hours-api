from PyInquirer import Separator, prompt, Validator, ValidationError
import hashlib
from View.abstract_vue import AbstractView
from Services.visiteur_services import VisiteurServices
import re
from Outils.fonctions_utiles import nom_format

specialCharacters = ['$', '#', '@', '!', '*']

class UsernameValidator(Validator):
    """ Permet de regarder si l'identifiant est valide
    """
    def validate(self,username):
        test = True
        if len(username.text) < 6 :
            test = False
        else :
            for caractere in username.text :
                if caractere in specialCharacters :
                    test = False
        if not test :
            raise ValidationError(message='Votre nom d\'utilisateur doit contenir au moins 6 caractères sans caractères spéciaux'
                                    , cursor_position=len(username.text))  # Move cursor to end

class PasswordValidator(Validator):
    """ Permet de regarder si le mot de passe est valide
    """
    def validate(self,password):
        test = True
        if len(password.text) <= 6 :
            test = False
        elif re.search('[0-9]',password.text) is None :
            test = False
        elif re.search('[A-Z]',password.text) is None or re.search('[a-z]',password.text) is None :
            test = False
        else :
            for caractere in password.text :
                if caractere in specialCharacters :
                    test = False
        if not test:
            message = "Mot de passe invalide : au moins 6 caractères / au moins un chiffre / au moins une majuscule et une minuscule / pas de caractères spéciaux"
            raise ValidationError(message=message, cursor_position=len(password.text))  # Move cursor to end

class RegisterView(AbstractView):

    def __init__(self):
        self.questions = [
            {
                'type': 'input',
                'name': 'username',
                'message': 'Choisissez votre nom d\'utilisateur : ',
                'validate': UsernameValidator

            },
            {
                'type': 'password',
                'name': 'password',
                'message': 'Choisissez votre mot de passe : ',
                'validate': PasswordValidator
            },
            {
                'type': 'input',
                'name': 'nom',
                'message': 'Quel est votre nom ? '
            },
            {
                'type': 'input',
                'name': 'prenom',
                'message': 'Quel est votre prenom ? '
            },
            {
                'type': 'input',
                'name': 'email',
                'message': 'Quel est votre email ? '
            } 
        ]

    def display_info(self):
        print('')

    def make_choice(self):
        answers = prompt(self.questions)
        password_hashed = hashlib.sha256(answers['password'].encode()).hexdigest()
        password = str(password_hashed)
        utilisateur = VisiteurServices.inscription(identifiant=answers['username']
                                                    , password=password
                                                    , nom=nom_format(texte=answers['nom'])
                                                    , prenom=nom_format(texte=answers['prenom'])
                                                    , email=answers['email'])
        AbstractView.session.user = utilisateur

        from View.regulier_view import RegulierView
        return RegulierView()