from PyInquirer import Separator, prompt
from View.abstract_vue import AbstractView
from Services.utilisateurs_services import UtilisateurServices
from Services.historique_services import HistoriqueServices
from Services.comptes_services import ComptesServices
from Services.suggestions_services import SuggestionsServices
from Services.recherche_services import RechercheServices
from Services.favoris_services import FavorisServices

class AdminView(AbstractView):
    def __init__(self):
        self.questions = [
            {
                'type': 'list',
                'name': 'actions',
                'message': 'Comment étancher votre soiffe ?',
                'choices': ['1. Trouver ma boisson :D'
                                , '2. Proposer ma nouvelle invention ;)'
                                , '3. Consulter les nouvelles inventions'
                                , '4. Consulter mes coups de coeurs <3'
                                , '5. Regarder les historiques de recherche ;)'
                                , '6. Consulter les comptes des utilisateurs'
                                , '7. Quitter l\'application :\'(' ]
            }
        ]
        self.actions = [
            {
                'type': 'list',
                'name': 'action',
                'message': 'Quel historique voulez-vous regarder ?',
                'choices': ['1. Mon mien !'
                            , '2. Celui de tous les utilisateurs ;)']
            }
        ]
        self.comptes = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Quel compte voulez-vous consulter ?',
                'choices': ['1. Mon mien !'
                            , '2. Celui des autres utilisateurs ;)']
            }
        ]

    @staticmethod
    def display_info():
        print('Oh, vous revoià {} ! \n'.format(AbstractView.session.user.prenom))

    def make_choice(self):
        reponse = prompt(self.questions)

        if reponse["actions"] == '1. Trouver ma boisson :D':
            AbstractView.session.recherche = RechercheServices()
            from View.recherche_view import RechercheView
            return RechercheView()

        elif reponse["actions"] == '2. Proposer ma nouvelle invention ;)':
            from View.suggerer_cocktail_view import SuggererCocktailView
            return SuggererCocktailView()

        elif reponse["actions"] == '3. Consulter les nouvelles inventions':
            AbstractView.session.suggestions = SuggestionsServices.get_all()
            if AbstractView.session.suggestions :
                AbstractView.session.mise_en_forme_suggestions = SuggestionsServices.mise_en_forme(suggestions=AbstractView.session.suggestions)
                from View.liste_suggestions_view import ListeSuggestionsView
                return ListeSuggestionsView()
            else :
                print('Il n\'y a aucune suggestions :\'( \n')
                return AdminView()

        elif reponse["actions"] == '4. Consulter mes coups de coeurs <3':
            AbstractView.session.user.chargement_favoris()
            if AbstractView.session.user.favoris_bdd or AbstractView.session.user.favoris_api :
                AbstractView.session.favoris = FavorisServices.chargement(AbstractView.session.user)
                AbstractView.session.mise_en_forme_favoris = FavorisServices.mise_en_forme(favoris=AbstractView.session.favoris)
                from View.favoris_view import FavorisView
                return FavorisView()
            else :
                print('Vos favoris sont vides :O \n')
                return AdminView()

        elif reponse["actions"] == '5. Regarder les historiques de recherche ;)':
            reponse = prompt(self.actions)

            if reponse['action'] == '1. Mon mien !':
                AbstractView.session.user.chargement_historique()
                if AbstractView.session.user.historique :
                    historique = AbstractView.session.user.historique
                    AbstractView.session.mise_en_forme_historique = HistoriqueServices.mise_en_forme(historique=historique)
                    from View.historique_view import HistoriqueView
                    return HistoriqueView()
                else :
                    print('Votre historique est vide :O \n')
                    return AdminView()

            elif reponse['action'] == '2. Celui de tous les utilisateurs ;)':
                historique = HistoriqueServices.get_all()
                if historique :
                    AbstractView.session.historique = historique
                    AbstractView.session.mise_en_forme_historique = HistoriqueServices.mise_en_forme(historique=historique)
                    from View.all_historique_view import AllHistoriqueView
                    return AllHistoriqueView()
                else :
                    print('Des cachotiers sont passés par là et l\'historique de l\'application est vide est vide :O \n')
                    return AdminView()

        elif reponse["actions"] == '6. Consulter les comptes des utilisateurs':
            reponse = prompt(self.comptes)

            if reponse['choix'] == '1. Mon mien !' :
                AbstractView.session.compte_utilisateur = AbstractView.session.user
                from View.compte_utilisateur_view import CompteUtilisateurView
                return CompteUtilisateurView()

            elif reponse['choix'] == '2. Celui des autres utilisateurs ;)' :
                AbstractView.session.comptes_utilisateurs = ComptesServices.get_all()
                indice = None
                for i in range(len(AbstractView.session.comptes_utilisateurs)) :
                    if AbstractView.session.comptes_utilisateurs[i].id == AbstractView.session.user.id :
                        indice = i
                del AbstractView.session.comptes_utilisateurs[indice]
                liste_comptes = AbstractView.session.comptes_utilisateurs
                AbstractView.session.mise_en_forme_comptes = ComptesServices.mise_en_forme(comptes_utilisateurs=liste_comptes)
                from View.comptes_utilisateurs_view import ComptesUtilisateursView
                return ComptesUtilisateursView()

        elif reponse["actions"] == '7. Quitter l\'application :\'(':
            from View.farewell_view import FarewellView
            return FarewellView()