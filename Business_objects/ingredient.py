class Ingredient():

    def __init__(self, id_ingredient, nom, mesure):
        """ Constructeur commun à tous les ingredients
            :param id_ingredient: id de l'ingredient
            :type id_ingredient: int
            :param nom: le nom de l'ingredient
            :type nom: string
            :param mesure: quantite et son unite d'un ingredient
            :type mesure: dict
        """
        self.id = id_ingredient
        self.nom = nom
        self.mesure = mesure