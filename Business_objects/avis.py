class Avis():

    def __init__(self, id_user, id_cocktail, note, commentaire):
        """ Constructeur commun à tous les avis
            :param id_user: id de l'utilisateur
            :type id_user: int
            :param id_cocktail: id du cocktail
            :type id_cocktail: int
            :param note: note du cokctail par un utilisateur
            :type note: float
            :param commentaire: commentaire du cocktail par un utilisateur
            :type commentaire: string
        """
        self.id_user = id_user
        self.id_cocktail = id_cocktail
        self.note = note
        self.commentaire = commentaire