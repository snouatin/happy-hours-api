import re
from DAO.historique_dao import HistoriqueDao
from DAO.favoris_dao import FavorisDao
from DAO.favoris_api_dao import FavorisApiDao

class Utilisateur():
    def __init__(self, id_user, TYPE_BASE_DE_DONNEE, identifiant, password, nom, prenom, email):
        """ Constructeur commun à tous les utilisateurs
            :param id_user: l'id de l'utilisateur
            :type id_user: int
            :param TYPE_BASE_DE_DONNEE: le type d'utilisateur
            :type TYPE_BASE_DE_DONNEE: string
            :param identifiant: l'identifiant de l'utilisateur
            :type identifiant: string
            :param password: le mot de passe de l'utilisateur
            :type password: string
            :param nom: le nom de l'utilisateur
            :type nom: string
            :param prenom: le prenom de l'utilisateur
            :type prenom: string
            :param email: l'email de l'utilisateur
            :type email: string 
        """
        self.id = id_user
        self.type = TYPE_BASE_DE_DONNEE
        self.favoris_bdd = None
        self.favoris_api = None
        self.historique = None
        self.identifiant = identifiant
        self.password = password
        self.nom = nom
        self.prenom = prenom
        self.email = email

    def chargement_favoris(self):
        """ Définit la methode qui va permettre de generer la liste 
            de favoris de l'utilisateur
        """
        self.favoris_bdd = FavorisDao.find_by_id_user(self.id)
        self.favoris_api = FavorisApiDao.find_by_id_user(self.id)

    def chargement_historique(self):
        """ Définit la methode qui va permettre de generer 
            l'historique de l'utilisateur
        """
        self.historique = HistoriqueDao.find_by_id_user(self.id)