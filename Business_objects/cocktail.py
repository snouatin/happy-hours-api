class Cocktail():
    
    def __init__(self, id_cocktail, type, nom, alcool, verre, ingredients, recette, URL, sauvegarde):
        """ Constructeur commun à tous les cocktails
            :param id_cocktail: l'id du cocktail
            :type id_cocktail: int
            :param nom: le nom du cocktail
            :type nom: string
            :param type: le type de cocktail
            :type type: string
            :param alcool: la conteneur en alcool du cocktail
            :type alcool: string
            :param verre: le type de verre pour boire le cocktail
            :type verre: string
            :param ingredients: les ingredients du cocktail
            :type ingredients: list <Ingredient>
            :param recette: la recette du cocktail
            :type recette: string
            :param recette: le lien de l'image du cocktail si il existe
            :type recette: string
            :param avis: les avis sur le cocktail
            :type avis: list
        """
        self.id = id_cocktail
        self.nom = nom
        self.type = type
        self.alcool = alcool
        self.verre = verre
        self.ingredients = ingredients
        self.recette = recette
        self.URL = URL
        self.avis = None
        self.sauvegarde = sauvegarde