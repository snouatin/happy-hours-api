# 🍻 Projet Informatique 2A: Happy Hour welcomes you ! 🍻


## 🍸 Courte présentation 🍸

Cette application a pour but principal de rechercher un cocktail stocké sur notre base de données ou bien sur une API. En vous inscrivant, vous aurez accès à de multiples fonctionnalités comme la création de votre propre cocktail et une liste de favoris personnalisées. 

N'hésitez pas, rejoignez nous dès maintenant !



## 🖥️ Run l'application 🖥️



### 💾 Installation de l'application 💾

Pour installer l'application, il vous suffit de taper:
-- git clone "https://gitlab.com/snouatin/happy-hours-api.git"



### 📃 Installation des dépendances 📃

Pour pouvoir utiliser l'application, il vous faudra installer les différents modules nécessaires à son bon fonctionnement. 

Tous ces modules seront contenus dans le fichier "requirements". 

* Comment les installer ? Utiliser la commande suivante: -- pip install -r "requirements"



### 🚴‍ Utilisation de l'application 🚴‍

Vous êtes désormais prêts à lancer l'application !

* Comment faire ? Il vous suffit de run le fichier "main.py" situé dans la racine du projet ! 

La commande suivante vous permettra aussi de lancer l'application :
-- python main.py



## 🦺 Classes métiers 🦺


### 🖱️ Objet métier : Utilisateur 🖱️


Vous pourrez trouver l'Utilisateur à l'adresse suivante : "https://gitlab.com/snouatin/happy-hours-api/-/tree/master/Business_objects/utilisateur.py"

P.S. Je ne parle pas de sa maison 😉



### 🧙‍ ‍T‍e‍s‍t‍s‍ ‍ ‍u‍n‍i‍t‍a‍i‍r‍e‍s‍ ‍:‍ ‍U‍t‍i‍l‍i‍s‍a‍t‍e‍u‍r‍ 🧙‍


Vous pourrez trouver les tests de l'Utilisateur à l'adresse suivante : "https://gitlab.com/snouatin/happy-hours-api/-/tree/master/Tests/Tests_Business_objects/test_business_objects_utilisateur.py"

P.S. Je ne parle pas de Covid-19 😉

#### Ce dépôt git est une copie de mon projet informatique réalisé en groupe en 2ème année
